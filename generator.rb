# Started on 2019-06-11
# My very own static site generator by Jeremy Sarchet

require "fileutils"
require "find"
require "pathname"
require "set"
require "yaml"

require "htmlbeautifier"

require_relative "metadata"
require_relative "series"
require_relative "gallery"


# For serving a default file when requesting a directory in different contexts
LOCAL_FILESYSTEM_POSTFIX = "index.html"
HTTP_SERVER_POSTFIX = ""
POSTFIX = HTTP_SERVER_POSTFIX

# Pattern for yaml front matter in html files
# The front matter is used by the site generator and stripped away
FRONT_MATTER_PATTERN = /---[\s\S]*?---\n/

# Pattern for substitution of fragments
INTERPOLATION_PATTERN = /%{[\S]*?}/

# Directories and their names for site generation
SOURCE = "./source" # the raw website content
FRAGMENTS = "./fragments" # common elements to be filled in via templating
WRAPPERS = "./wrappers" # makes a complete html document when content inserted
BUILD = "./build" # where the generated static website is output

# NOTE: There is not any sort of cleanup of the build directory.
# If the source contents and/or generation procedure changes to as to
# "orphan" anything in the build directory, then those orphan(s) should
# be deleted manually, or the entire contents of the build directory
# should be deleted, and the whole thing updated anew.

def make_way(path)
	# Preparation for file overwrite. Creates intermediate directories as
	# needed, and deletes the file if it exists.
	FileUtils.makedirs(File.dirname(path))
	File.delete(path) if File.exist?(path)
end

def update(src, dst)
	# Copies a file from src to dst paths, overwriting, and creating
	# intermediate directories as needed
	make_way(dst)
	FileUtils.copy_file(src, dst)
	puts "Updated:    #{src}\n"
end

def resolve(name, meta)
	fragment_path = "#{FRAGMENTS}/#{name}.html"
	dirname = File.dirname(meta['path'])
	if name == "listing"
		result = Series.generate_listing(meta["path"])
	elsif name == "listing-no-thumbnails"
		result = Series.generate_listing(meta["path"], with_thumbnails=false)
	elsif name == "jumps"
		result = Series.generate_jumps(meta["path"])
	elsif name == "jumps-no-thumbnails"
		result = Series.generate_jumps(meta["path"], with_thumbnails=false)
	elsif name == "gallery"
		fragment = File.read(fragment_path)
		result = Gallery.generate(fragment, meta["path"])
	elsif name == "feature"
		feature_path = File.join(dirname, "feature.html")
		result = File.file?(feature_path) ? File.read(feature_path) : "feature.html not found in #{dirname}"
	elsif meta.key?(name)
		result = meta[name]
	elsif File.file?(fragment_path)
		result = File.read(fragment_path)
	else
		result = "UNRESOLVED FRAGMENT: `#{name}`"
		warn(result)
	end
	return result.to_s
end

def unresolved?(document)
  return INTERPOLATION_PATTERN.match?(document.to_s)
end

def resolve_recursive(document, meta)
	if unresolved?(document)
		placeholders = document.scan(INTERPOLATION_PATTERN)
		placeholders = Set.new(placeholders)  # unsure why this is necessary

		placeholders.each do |placeholder|
			name = document[INTERPOLATION_PATTERN][2..-2]
			fragment = resolve(name, meta)
			resolved_fragment = resolve_recursive(fragment, meta)
			document = document.gsub(/%{#{name}}/, resolved_fragment)
		end
	end
	return document
end

def apply_wrapping_recursive(document, wrapping="base")
	meta, content = Metadata.split_and_parse_front_matter(document)

	# Default values for certain keys if not explicitly provided
	meta["description"] ||= ""
	meta["meta-image"] ||= ""

	if meta.key?("wrapping")
		name = meta["wrapping"]
		_, content = apply_wrapping_recursive(content, name)
	end

	wrapper = File.read("#{WRAPPERS}/#{wrapping}.html")
	return meta, wrapper.gsub(/%{content}/, content)
end

def upgrade_html(source_file_path, build_file_path)
	puts "Generating: #{build_file_path}"

	source_file_dir = Pathname.new(source_file_path).dirname
	path_to_root = Pathname.new(SOURCE).relative_path_from(source_file_dir)
	location = Pathname.new(source_file_dir).relative_path_from(SOURCE)
	url = "https://www.jeremysarchet.com/" + location.to_s

	# Read the file. Done only once per source file
	document = File.read(source_file_path)

	# Insert the content into wrapping recursively
	meta, wrapped_content = apply_wrapping_recursive(document)

	# Add in some extra metadata
	meta.merge!( {"path" => source_file_path.to_s,
								"postfix" => POSTFIX,
								"root" => path_to_root.to_s,
								"url" => url,
								} )

	# If it's the error.html page, use absolute paths so links work even if user
	# requests a nonexistant URL not at the root level.
	if source_file_path == "./source/error.html"
		meta["root"] = ""
	end

	# Apply the filling to the content and continue recursively
	output = resolve_recursive(wrapped_content, meta)

	# Beautify the output
  output = HtmlBeautifier.beautify(output)

	existing_content = ""

	if File.file?(build_file_path)
		existing_file = File.open(build_file_path, "r")
		existing_content = existing_file.read[..-2] # Seems it's necessary to trim the last character before comparision. Newline character, I guess?
		existing_file.close
	end

	# If necessary, write the generated file to the build directory
	unless output == existing_content
		puts "Updated:    #{build_file_path}"
		make_way(build_file_path)
		new_file = File.open(build_file_path, "w")
		new_file.puts(output)
		new_file.close
	end
end

# Walk the source filetree and get all the non-hidden files
file_paths = Find.find(SOURCE).reject{ |path|
	File.directory?(path) || File.basename(path)[0] == "."
}

# Update the build filetree
file_paths.each do |file_path|
	build_file_path = Pathname.new(file_path).sub(SOURCE, BUILD)

	# If it's an index.html file, update it every time. This is a lightweight
	# operation anyways. Decision is that it's not worth it and doing
	# a check will be tricky as these files are modified (templating is
	# applied) in the process of site generation
	if (file_path.match(/index\.html\Z/) || file_path.match(/error\.html\Z/))
		# Apply templates
		upgrade_html(file_path, build_file_path)

	# If it's not an html file, check before updating it in the build
	# to save the work of overwriting large assets unnecessarily
	else
		# Has the source file been modified more recently than the build file?
		unless FileUtils.uptodate?(build_file_path, [file_path])
			# If so, update the build file
			update(file_path, build_file_path)
		end
	end
end

# Cleanup files that should no longer be in the build directory
source_paths = Find.find(SOURCE).reject{ |path| File.basename(path)[0] == "."}
build_paths = Find.find(BUILD).to_a

build_paths.each do |path|
	test_path = Pathname.new(path).sub(BUILD, SOURCE).to_s
	unless source_paths.include? test_path
		puts "Deleting:   #{path}"
		File.delete(path) if File.file?(path)
		FileUtils.rm_r(path) if File.directory?(path)
	end
end

# Pass in "--dryrun" as the first parameter to perform a dry run

# Reference: https://www.abstrys.com/tech-aws-publish-s3-website.html

build_dir="build"
bucket="s3://www.jeremysarchet.com"

# Call the sync command and capture the output
RESULT=($(aws s3 sync $1 \
  --acl public-read \
  $build_dir \
  $bucket \
  --output json
))
# Notes on aws s3 sync
# Exclusions:
# --exclude "*.html" \
# --exclude "*.js" \
# --exclude "*.css" \
#
# This pattern works for e.g. doing only files ending in .html
# --exclude "*" --include "*.html"


# Derive an array of paths that were synced
paths=()
for i in "${RESULT[@]}"
do
  if [[ $i == s3* ]]
  then
    path=${i:26} # substring after "s3://www.jeremysarchet.com"
    paths+=($path)
    echo "Syncing: ${path}"
  fi
done

# Proceed to cache invalidation unless this is a dry run
if [[ $1 == "--dryrun" ]]; then
  echo "This was only a dry run, exiting."
  exit 0
fi

# Make an invalidation for these paths
echo "Invalidation:"
aws cloudfront create-invalidation \
    --distribution-id $CLOUDFRONT_ID \
    --paths "${paths[@]}"

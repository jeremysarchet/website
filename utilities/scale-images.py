"""Generate jpgs of a range of sizes"""

import os
from glob import glob

from PIL import Image, ImageOps

# source_dir = '/Users/jbs/Dropbox/website/assets/2019-07-16-deanimation-framed-prints-images'
# base_dir = "/Users/jbs/website/source/deanimation/framed-prints/images"
# output_sizes = ["0240", "0480", "0640", "0768", "1024", "1366", "1600", "1920", "full"]

# source_dir = "/Users/jbs/Dropbox/website/assets/2018-09-15-frame-making"
# base_dir = "/Users/jbs/website/source/blog/2018-11-20-frame-making/images/"
# output_sizes = ["0240", "0480", "0640", "0768", "1024", "1366", "1600", "1920"]

# source_dir = "/Users/jbs/Dropbox/website/assets/2019-07-16-deanimation-tuolumne-images"
# base_dir = "/Users/jbs/website/source/deanimation/tuolumne/images"
# output_sizes = ["0240", "0480", "0640", "0768", "1024", "1366", "1600", "1920"]

# source_dir = "/Users/jbs/Dropbox/website/assets/2019-09-05-deanimation-sneak-peek-images"
# base_dir = "/Users/jbs/website/source/deanimation/muga/images"
# output_sizes = ["0240", "0480", "0640", "0768", "1024", "1366", "1600", "1920"]

# source_dir = "/Users/jbs/Dropbox/website/assets/2019-10-04-deanimation-tudela-images"
# base_dir = "/Users/jbs/website/source/deanimation/tudela/images"
# output_sizes = ["0240", "0480", "0640", "0768", "1024", "1366", "1600", "1920"]

# source_dir = "/Users/jbs/Dropbox/website/assets/2019-10-04-deanimation-mono-images"
# base_dir = "/Users/jbs/website/source/deanimation/mono/images"
# output_sizes = ["0240", "0480", "0640", "0768", "1024", "1366", "1600", "1920"]

# source_dir = "/Users/jbs/Dropbox/website/assets/2019-10-12-oscilloscope/poster-frames"
# base_dir = "/Users/jbs/website/source/blog/oscilloscope-animation/images"
# output_sizes = ["0240", "0480", "0640"]

# source_dir = "/Users/jbs/Dropbox/website/assets/2020-01-27-deanimation-tamalpais-poster-images"
# base_dir = "/Users/jbs/website/source/deanimation/tamalpais/images"
# output_sizes = ["0240", "0480", "0640", "0768", "1024", "1366", "1600", "1920"]

# source_dir = "/Users/jbs/Dropbox/website/assets/2020-09-14-deanimation-moklumne-poster-images"
# base_dir = "/Users/jbs/website/source/deanimation/mokelumne/images"
# output_sizes = ["0240", "0480", "0640", "0768", "1024", "1366", "1600", "1920"]

source_dir = "/Users/jbs/Dropbox/website/assets/2020-11-30-deanimation-berkeley-poster-images"
base_dir = "/Users/jbs/website/source/deanimation/berkeley/images"
output_sizes = ["0240", "0480", "0640", "0768", "1024", "1366", "1600", "1920"]



jpg_paths = glob(os.path.join(source_dir, "*.jpg"))
png_paths = glob(os.path.join(source_dir, "*.png"))
image_paths = jpg_paths + png_paths


def process(image, output_size, output_path):
    output_image = image.copy()
    if output_size != "full":
        output_image.thumbnail((int(output_size), int(output_size)), Image.ANTIALIAS)
        # image = image.resize((200,200), Image.ANTIALIAS)
    output_image.save(output_path, optimize=True, quality=85, progression=False)
    msg = "Processed: {} {}".format(output_size, output_path)
    print(msg)


for output_size in output_sizes:
    output_dir = os.path.join(base_dir, output_size)
    os.makedirs(output_dir, exist_ok=True)


for image_path in image_paths:
    output_basename = os.path.basename(image_path)
    image = Image.open(image_path)
    original_size = image.size

    if output_basename.endswith(".png"):
        output_basename = output_basename.replace(".png", ".jpg")
        image = image.convert("RGB")

    for output_size in output_sizes:
        output_dir = os.path.join(base_dir, output_size)
        output_path = os.path.join(output_dir, output_basename)
        process(image, output_size, output_path)


"""
In the future do something like this for featured image thumbnails
[["75x50", [75, 50],
["150x100", 150, 100],
["300x150", [300, 150],]
image = ImageOps.fit(image, (150, 100), Image.ANTIALIAS)
"""

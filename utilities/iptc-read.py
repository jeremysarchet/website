# Read the IPTC info from a directory of images and make a YAML file for use
# in a gallery
#
# Usage: python3 iptc-read <input-dir> <output-dir>

import os
import sys
from iptcinfo3 import IPTCInfo

INPUT_DIR = sys.argv[1]
OUTPUT_FILE = os.path.join(INPUT_DIR, "info.txt")

output = open(OUTPUT_FILE, "w")

output.write("---\n")

with os.scandir(INPUT_DIR) as root_dir:
    for path in root_dir:
        if path.is_file() and str(path.name).lower().endswith((".jpg", ".png")):
            info = IPTCInfo(os.path.join(INPUT_DIR, path.name))
            title = info["headline"].decode("utf-8") if info["headline"] else '""'
            description = info["caption/abstract"].decode("utf-8") if info["caption/abstract"] else '""'

            output.write("- title: " + title + "\n")
            output.write("  imagefile: " + path.name + "\n")
            output.write("  description: " + description + "\n")

output.close()


import os
import boto3

files = [

# "index.html",

# "about/index.html",

# "contact/index.html",

# "role-models/index.html",

# "blog/index.html",
# "blog/bubble-with-ninety-sticks/index.html",
# "blog/eight-rod-ring/index.html",
# "blog/eight-level-dowel-tower/index.html",
# "blog/groutfiti/index.html",
# "blog/autological/index.html",
# "blog/so-percussion/index.html",
# "blog/a-tree-vibrates/index.html",
# "blog/SCIART-2012/index.html",
# "blog/lights-on-a-kite/index.html",
# "blog/katalonski/index.html",
# "blog/frame-making/index.html",
# "blog/camera-comparisons/index.html",
# "blog/video-box/index.html",
# "blog/plate-map-generator/index.html",
# "blog/phonemic-tercets/index.html",
# "blog/real-make-believe-space-programs/index.html",
# "blog/oscilloscope-animation/index.html",
# "blog/tensegrity-explained/index.html",
# "blog/power-pendulum/index.html",
# "blog/seamless-mobius-weave/index.html",
# "blog/thanksgiving-in-the-snow/index.html",
# "blog/check-out-martin-kimbell/index.html",
# "blog/carving-wooden-pliers/index.html",
# "blog/weightless-wreath/index.html",
# "blog/journey-in-the-tufa-kingdom/index.html",
# "blog/topology-rocks/index.html",
# "blog/high-sierra-trail/index.html",
# "blog/rotate-and-circumscribe/index.html",
# "blog/image-blending-improvements/index.html",

# "deanimation/index.html",
# "deanimation/framed-prints/index.html",
# "deanimation/muga/index.html",
# "deanimation/tudela/index.html",
# "deanimation/mono/index.html",
# "deanimation/tuolumne/index.html",
# "deanimation/tamalpais/index.html",
# "deanimation/mokelumne/index.html",
# "deanimation/berkeley/index.html",

# "floating-compression/index.html",
# "floating-compression/calling-ball/index.html",
# "floating-compression/golden-bamboo-column/index.html",

# "kinetic-sculpture/index.html",
# "kinetic-sculpture/springy-mobile/index.html",
# "kinetic-sculpture/make-a-ripple/index.html",
# "kinetic-sculpture/rhomboid/index.html",

# "songwriting/index.html",
"songwriting/i-fell-over/index.html",

# "scripts/gallery.js",
# "styles/base.css",
# "styles/gallery.css",

]

# Create an S3 client
s3 = boto3.client('s3')

bucket_name = 'www.jeremysarchet.com'
source_dir = "/Users/jbs/website/build"

print("Overwriting the following items in bucket: `{}`".format(bucket_name))
for file in files:
    source_path = os.path.join(source_dir, file)
    destination_path = file

    content_type = "text/html"
    if file.endswith(".css"):
        content_type = "text/css"
    elif file.endswith(".js"):
        content_type = "text/javascript"

    s3.upload_file(source_path, bucket_name, destination_path, ExtraArgs={'ACL': 'public-read', 'ContentType': content_type})


    print(file)

# S3 Redirection routing from certain URLs from my old website to the
# corresponding URLs on the current website

redirects = [["portfolio/2017-001-001",         "deanimation/tuolumne"],
["portfolio/2017-001-002",                      "deanimation/tuolumne"],
["portfolio/2017-001-003",                      "deanimation/tuolumne"],
["portfolio/2017-001-004",                      "deanimation/tuolumne"],
["portfolio/2017-001-005",                      "deanimation/tuolumne"],
["portfolio/2017-001-006",                      "deanimation/tuolumne"],
["portfolio/2017-001-007",                      "deanimation/tuolumne"],
["portfolio/2017-001-008",                      "deanimation/tuolumne"],
["portfolio/2017-001-009",                      "deanimation/tuolumne"],
["portfolio/2017-001-010",                      "deanimation/tuolumne"],
["portfolio/2017-001-011",                      "deanimation/tuolumne"],
["portfolio/2017-001-012",                      "deanimation/tuolumne"],
["portfolio/2017-001-013",                      "deanimation/tuolumne"],
["portfolio/2017-001-014",                      "deanimation/tuolumne"],
["portfolio/2017-001-015",                      "deanimation/tuolumne"],
["portfolio/rhomboid",                          "kinetic-sculpture/rhomboid"],
["portfolio/make-a-ripple",                     "kinetic-sculpture/make-a-ripple"],
["portfolio/springy-mobile",                    "kinetic-sculpture/springy-mobile"],
["portfolio/calling-ball",                      "floating-compression/calling-ball"],
["portfolio/golden-bamboo-column",              "floating-compression/golden-bamboo-column"],
["2019/05/22/heres-how-i-chose-my-camera",      "blog/camera-comparisons"],
["blog/2019-05-22-camera-comparisons",          "blog/camera-comparisons"],
["extras/camera-envelopes",                     "blog/camera-comparisons"],
["2018/05/17/katalonski",                       "blog/katalonski"],
["2016/12/29/topology-rocks",                   "blog/topology-rocks"],
["2016/12/27/journey-in-the-tufa-kingdom",      "blog/journey-in-the-tufa-kingdom"],
["2016/12/08/weightless-wreath",                "blog/weightless-wreath"],
["2016/10/20/carving-wooden-pliers",            "blog/carving-wooden-pliers"],
["2016/10/17/real-make-believe-space-programs", "blog/real-make-believe-space-programs"],
["2016/05/12/check-out-martin-kimbell",         "blog/check-out-martin-kimbell"],
["2015/11/30/thanksgiving-in-the-snow",         "blog/thanksgiving-in-the-snow"],
["2015/10/17/lights-on-a-kite",                 "blog/lights-on-a-kite"],
["2015/07/26/seamless-mobius-weave",            "blog/seamless-mobius-weave"],
["2013/06/14/power-pendulum",                   "blog/power-pendulum"],
["2013/06/06/tensegrity-explained",             "blog/tensegrity-explained"],
["2012/11/15/sciart-2012",                      "blog/SCIART-2012"],
["2012/10/10/a-tree-vibrates",                  "blog/a-tree-vibrates"],
["2012/08/12/oscilloscope-animation",           "blog/osciloscope-animation"],
["2011/11/01/so-percussion",                    "blog/so-percussion"],
["2008/03/07/autological",                      "blog/autological"],
["2007/10/10/groutfiti",                        "blog/groutfiti"],
["2007/06/15/eight-level-dowel-tower",          "blog/eight-level-dowel-tower"],
["2007/03/02/eight-rod-ring",                   "blog/eight-rod-ring"],
["2007/02/17/bubble-with-ninety-sticks",        "blog/bubble-with-ninety-sticks"],
["deanimation/river-flow",                      "deanimation/tuolumne"],
["deanimation/preview",                         "deanimation/muga"],
["deanimation/sneak-peek",                      "deanimation/muga"],
["deanimation/prints",                          "deanimation/framed-prints"],
["say-yah-to-da-up-eh",                         "songwriting/say-yah-to-da-up-eh"]
]


result = ""
result << "<RoutingRules>\n\n"

redirects.each do |redirect|
  result << "  <RoutingRule>\n"
  result << "    <Condition>\n"
  result << "      <KeyPrefixEquals>#{redirect[0]}</KeyPrefixEquals>\n"
  result << "    </Condition>\n"
  result << "    <Redirect>\n"
  result << "      <HostName>jeremysarchet.com</HostName>\n"
  result << "      <ReplaceKeyWith>#{redirect[1]}</ReplaceKeyWith>\n"
  result << "    </Redirect>\n"
  result << "  </RoutingRule>\n\n"
end

result << "</RoutingRules>\n"

puts result

"""Utility script to generate post thumbnails.

If the `thumbnail` directory already exists it won't overwrite.
"""

import os
from glob import glob

from PIL import Image, ImageOps

base_dir = "/Users/jbs/website/source"

posts = [
["blog/video-box", "2020-01-11-angled-portrait.jpg", (0.5, 0.25)],
["blog/rotate-and-circumscribe", "iteration-poster.jpg"],
["blog/a-tree-vibrates", "2012-10-10-a-tree-vibrates-header.png"],
["blog/autological", "2016-11-14-autological-word-chart.jpg"],
["blog/bubble-with-ninety-sticks", "2008-02-18-bubble-with-sixty-sticks-pentagon.jpg", (0.6, 0)],
["blog/camera-comparisons", "plot-screenshot.png", (0, 0)],
["blog/carving-wooden-pliers", "2016-10-20-wood-pliers-0.jpg"],
["blog/check-out-martin-kimbell", "martin-kimbell-lights-jumping-in-forest.jpg", (1, 0)],
["blog/eight-level-dowel-tower", "eight-level-dowel-tower-portrait.jpg"],
["blog/eight-rod-ring", "2008-02-18-wreath-of-eight-rods-top.jpg"],
["blog/frame-making", "images/1920/2018-10-09 15.57.41.jpg"],
["blog/groutfiti", "2016-11-14-groutfiti.jpg", (0.4, 0)],
["blog/high-sierra-trail", ""],
["blog/journey-in-the-tufa-kingdom", "2016-12-27-savina-jeremy-mono-lake.jpg", (1, 0)],
["blog/katalonski", "2018-05-17-katalonski-poster-frame.png"],
["blog/lights-on-a-kite", "lights-on-a-kite-1.jpg", (1, 0)],
["blog/oscilloscope-animation", "images/0640/16.jpg", (1, 0)],
["blog/power-pendulum", "2013-06-14-self-portrait-under-power-tower.jpg", (0.4, 0)],
["blog/phonemic-tercets", "thuja-plicata.jpg"],
["blog/real-make-believe-space-programs", "2016-05-12-mission-control-booster-separation.png", (0.25, 0)],
["blog/SCIART-2012", "sciart-2012-panel-discussion.png"],
["blog/seamless-mobius-weave", "2016-12-12-seamless-mobius-weave.jpg"],
["blog/so-percussion", "so-percussion-media.jpg"],
["blog/tensegrity-explained", "2013-03-31-calling-ball-detail.jpg", (0, 0)],
["blog/thanksgiving-in-the-snow", "2015-11-28 14.16.39.jpg"],
["blog/topology-rocks", "2016-12-29-savina-jeremy-arch.jpg", (0.8, 0)],
["blog/weightless-wreath", "2016-12-08-weightless-wreath-top.jpg"],
["deanimation/framed-prints", "2017-09-29-0579-framed-square.jpg"], # customized
["deanimation/tuolumne", "images/1920/2017.09.29.0579.jpg", (1, 0)],
["deanimation/mono", "images/1920/2017-10-02-0892-accumulation-n2-poster.jpg", (0.7, 0)],
["deanimation/tudela", "images/1920/2018-04-02-0573-accumulation-n4-poster.jpg", (1, 0)],
["deanimation/muga", "images/1920/2019-08-27-0330-tail-n4-t40-poster.jpg", (0.9, 0)],
["deanimation/tamalpais", "images/1920/2019-12-31-0503-tail-n2-t120-crossfaded-poster.jpg", (1, 0)],
["floating-compression/calling-ball", "2013-03-31-calling-ball-featured.jpg"],
["floating-compression/golden-bamboo-column", "2017-12-20-gbc-portrait.jpg"],
["kinetic-sculpture/rhomboid", "2019-02-06-rhomboid.jpg"],
["kinetic-sculpture/make-a-ripple", "2018-01-03-mar-portrait-perspective-corrected-web-full.jpg"],
["kinetic-sculpture/springy-mobile", "2017-11-11-springy-mobile-portrait.jpg"],
["blog/plate-map-generator", "well-plate-icon.png"],
["deanimation/mokelumne", "images/1920/2020-01-20-0162-m1-s1-i150-poster.jpg"],
["deanimation/berkeley",  "images/1920/2020-09-23-0303-m1-s1-i200-poster.jpg", (0.0, 0.0)],
["blog/image-blending-improvements", "2017-07-09-9488-detail.png"],
]

output_sizes = [90, 180, 270]

for post in posts:
    post_dir, image_filename = post[0], post[1]
    centering = post[2] if len(post) > 2 else (0.5, 0.5)
    image_path = os.path.join(base_dir, post_dir, image_filename)

    if not (image_path.endswith(".png") or image_path.endswith(".jpg")):
        continue

    thumbnail_dir = os.path.join(base_dir, post_dir, "thumbnail")
    if os.path.exists(thumbnail_dir):
        continue

    image = Image.open(image_path)

    if image_path.endswith(".png"):
        image = image.convert("RGB")

    for output_size in output_sizes:
        output_dir = os.path.join(thumbnail_dir, str(output_size))
        os.makedirs(output_dir)

        output_path = os.path.join(output_dir, "post-thumbnail.jpg")
        output_image = ImageOps.fit(image, (output_size, output_size), Image.ANTIALIAS, centering=centering)
        output_image.save(output_path, optimize=True, quality=85, progression=True)

    print("Generated thumbnails for: {}".format(post_dir))


content = document.getElementById("content");
sidebar = document.getElementById("sidebar");
viewer = document.getElementById("viewer");
artworkArea = document.getElementById("artwork-area");
controls = document.getElementById("controls");
description = document.getElementById("description");


var toggleSidebarButton = document.getElementById("toggle-sidebar-button");
toggleSidebarButton.onclick = function() {toggleSidebar()};

function toggleSidebar() {
  if (sidebar.style.display === "none") {
    sidebar.style.display = "table-cell";
    toggleSidebarButton.innerHTML = "Hide Thumbnails"
  } else {
    sidebar.style.display = "none";
    toggleSidebarButton.innerHTML = "Show Thumbnails"
  }
}

var button = document.getElementById("fullscreen-button");
button.onclick = function() {openFullscreen(content)};


function openFullscreen(elem) {
  if (elem.requestFullscreen) {
    elem.requestFullscreen();
  } else if (elem.mozRequestFullScreen) { /* Firefox */
    elem.mozRequestFullScreen();
  } else if (elem.webkitRequestFullscreen) { /* Chrome, Safari and Opera */
    elem.webkitRequestFullscreen();
  } else if (elem.msRequestFullscreen) { /* IE/Edge */
    elem.msRequestFullscreen();
  }
}

function closeFullscreen() {
  if (document.exitFullscreen) {
    document.exitFullscreen();
  } else if (document.mozCancelFullScreen) { /* Firefox */
    document.mozCancelFullScreen();
  } else if (document.webkitExitFullscreen) { /* Chrome, Safari and Opera */
    document.webkitExitFullscreen();
  } else if (document.msExitFullscreen) { /* IE/Edge */
    document.msExitFullscreen();
  }
}


ARTWORK_HEIGHT_FRACTION = 0.65;

artwork = artworkArea.getElementsByClassName("artwork")[0];

function updateLayout() {
  artworkAspect = aspect(artwork);
  viewerAspect = viewer.offsetWidth / (viewer.offsetHeight * ARTWORK_HEIGHT_FRACTION);
  viewerWidth = viewer.offsetWidth;
  viewerHeight = viewer.offsetHeight;
  controlsHeight = controls.offsetHeight;

  // Place the artwork
  if (viewerAspect > artworkAspect) {
    // The artwork is height constrained, must preserve the bottom 25%
    artworkAreaHeight = viewerHeight * ARTWORK_HEIGHT_FRACTION;
    artworkAreaWidth = (viewerHeight * ARTWORK_HEIGHT_FRACTION * artworkAspect);
  } else {
    // The artwork is width constrained, allowing for the lower area to expand
    artworkAreaWidth = viewerWidth;
    artworkAreaHeight = (viewerWidth / artworkAspect);
  }
  artworkArea.style.width = artworkAreaWidth + "px";
  artworkArea.style.height = artworkAreaHeight + "px";
  artworkArea.style.left = ((viewerWidth - artworkAreaWidth) / 2) + "px";

  // Place the controls
  controls.style.top = artworkAreaHeight + "px";
  controls.style.width = viewerWidth + "px";

  // Place the description
  description.style.top = (artworkAreaHeight + controlsHeight) + "px";
  description.style.height = (viewerHeight - artworkAreaHeight - controlsHeight) + "px";
  description.style.width = viewerWidth + "px";
}

function aspect(artwork) {
  if (artwork.nodeName == "IMG") {
    return artwork.naturalWidth / artwork.naturalHeight;
  } else if (artwork.nodeName == "VIDEO") {
    return artwork.videoWidth / artwork.videoHeight;
  } else {
    return 1.0;
  }
}

window.onload = updateLayout;
setInterval(function(){updateLayout();}, 1);

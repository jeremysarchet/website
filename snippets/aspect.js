
function aspect(artwork) {
  if (artwork.nodeName == "IMG") {
    return artwork.naturalWidth / artwork.naturalHeight;
  } else if (artwork.nodeName == "VIDEO") {
    return artwork.videoWidth / artwork.videoHeight;
  } else {
    return 1.0;
  }
}

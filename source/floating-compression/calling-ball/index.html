---
title: Calling Ball
date: 2008-01-17
wrapping: post
---

<p>This sculpture holds a special place in my heart. It's all tied up with the
moment in my life when I realized I wanted to make art for a living.</p>

<figure class="full-width">
<img src="2013-03-31-calling-ball-featured.jpg">
<figcaption><strong>Calling Ball (2008)</strong><br>
Poplar dowel, roofing nails, twisted nylon masonry twine<br>
33-inch diameter</figcaption>
</figure>

<video class="full-width" autoplay="" loop="" muted="" playsinline="" preload=""
src="2017-12-12-cb-splash-loopable-1080x1080.mp4"
poster="2017-12-12-cb-splash-loopable-1080x1080-poster.png">
</video>

<p>It's built with sixty rods and one hundred and eighty lengths of string. The
whole thing is light and airy, yet sturdy and springy. It's a lot of fun to
look at and to play with.<p>

<p>The structure is based on Buckminster Fuller's sculpture "Sixty-Strut
Tensegrity Sphere". I was able to reverse-engineer Fuller's sculpture by
studying a few photographs and puzzling through it. Once I had the geometry
figured out, the rest was straightforward. I fabricated a bundle of struts
(wooden rods with nails on the ends), and a collection of tendons (pre-cut
lengths of nylon twine with the ends melted to form stoppers) and began
making connections. The assembly process was fast and smooth, I was done in a
weekend, and just like that, I had a model of Fuller's marvelous sculpture of
my own. What a delight!</p>

<h2>The making of:</h2>

<p>When I first came across "Sixty-Strut Tensegrity Sphere" online. I was blown
away. Astounded. Captivated. I couldn't get it out of my mind.</p>

<figure class="partial-width-centered">
<img src="60-strut-tensegrity-sphere-fuller.jpg">
<figcaption>
<strong>Sixty-Strut Tensegrity Sphere (1979)</strong><br>
Richard Buckminster Fuller<br>
(American, 1895-1983)<br>
Stainless steel and wire 9 ft. diameter<br>
<br>
Photo credit unknown. It appears to be taken with the sculpture on exhibit at
the <a href="http://www.solwaygallery.com/" target="_blank">Carl Solway Gallery</a>.
</figcaption>
</figure>

<p>I studied all the information I could get on this sculpture, which was
scarce. Here's what I found online:</p>

<ul>
<li><a href="http://www.publicart.wisc.edu/artists-fuller-sixty-strut-tensegrity-sphere.htm" target="_blank">
Description of the sculpture at the University of Wisconsin-Madison</a></li>
<li><a href="http://www.engr.wisc.edu/news/exhibits/Tensegrity/" target="_blank">
Photos of installation at UW-Madison</a></li>
<li><a href="https://tensegrity.wikispaces.com/60+Struts" target="_blank">
Article from Tensegrity Wikispace</a></li>
<li><a href="http://www.tensileworld.com/forum/viewtopic.php?t=16" target="_blank">
Forum discussion at TensileWorld</a></li>
</ul>

<p>I stared long and hard at the photos of this sculpture. I wanted to see it
for myself in real life, get up close, find out how it behaves, how the pieces
go together, play with it, get my hands on it.</p>

<p>But even if I contacted the management of the building at UW-Madison where it
hangs, bought a plane ticket and flew out there, I'd have to view it from the
appropriate distance for the general public. I thought my enthusiasm warranted
more than that. What I really wanted was to have Fuller's sculpture for myself,
but this is unrealistic. I'm sure UW-Madison likes having it very much.</p>

<p>So, if I can't buy it or lease it, the next best thing is to make my own.
As Tom Sachs says, "making it is a way of having it".</p>

<h3>The known</h3>

<p>From my research I knew that the sculpture was built with repeated three-prism
modules. As a self-taught tensegrity maker, I've got this part down. A
three-prism module looks like this:</p>

<div style="display:table; margin:-0.5em;">
	<div style="display:table-row">
		<div style="display:table-cell; padding:0.5em;">
			<img class="full-width" src="2008-02-10-three-prism-top.jpg">
		</div>
		<div style="display:table-cell; padding:0.5em;">
			<img class="full-width" src="2008-02-10-three-prism-side.jpg">
		</div>
	</div>
</div>

<p>Fuller's sculpture specifically consists of twenty of these modules in a
spherical arrangement. That accounts for the sixty struts, (20 x 3 = 60). But
on the question of just what type of arrangement, I was stuck.</p>

<h3>The unknown</h3>

<p>I spent many weeks thinking about how twenty of these things could possibly
go together to make a resulting sculpture like Fuller's. But I just couldn't
see how to connect them.</p>

<p>For a proper tensegrity, none of the struts should be touching. How could
this be achieved here? The above linked article from UW-Madison says the
modules are arranged in an "icosahedral form". But, if I merely aligned the
triangular bases with the faces of the regular icosahedron, then the ends of
adjacent struts would touch each other. You can see this is the case in the
below drawing.</p>

<img class="partial-width-centered" src="2016-07-05-regular-icosahedron-diagram.png">

<p>So a regular icosahedron wouldn't do at all. I was puzzled and it really
troubled me that I couldn't see how it went together. If only I could get a
high resolution image, or travel to see the sculpture in person.</p>

<p>But I was determined to discover the secret with the limited resources at
hand. I felt this was part of the challenge. If planetary scientists were
figuring out the geology of distant worlds using only low resolution images,
surely I could figure out how these sticks and string go together.</p>

<h3>Finding out the secret</h3>

<p>The solution came to me at some point over this past winter holiday. I got
to thinking about the chirality of tensegrity, how in many structures the
elements go together with a twist. (You can read more about that in another
post <a href="%{root}/blog/2007-02-17-bubble-with-ninety-sticks/%{postfix}" target="_blank">
here</a>).</p>

<p>I thought, what if I twisted the triangular faces of the icosahedron, and
separated them a bit so they were partially overlapping? That way, each
triangular base would be shifted and and the vertices would all be staggered.
Could that be the trick? That had to be the trick! I was sure of it and
instantly I could see the whole thing in my mind. Doggone it, even if it wasn't
how Fuller's sculpture goes together, it will be how mine goes together, I
thought.</p>

<p>Here's an animation of the overlapping triangles as a spherical tiling. It
is a metamorphosis from an icosidodecahedron and back again. This is what I saw
in my mind's eye. Twenty triangles connected together on the sphere, elbow to
elbow.</p>

<img class="partial-width-centered" src="icosadodecahedron-twist-morph-animated.gif">

<p>I wasted no time in starting construction. When I had the struts fabricated
and the lengths of the tendons figured out with trial and error, I was off and
assembling modules. It took me two sessions over two days and then I had it.</p>

<p>Seeing the sculpture come together was pure magic, and I was elated to see
my twisted icosahedron theory be proven right.</p>

<img class="partial-width-centered" src="calling-ball-schematic.png">

<p>Here's a schematic of how the bases of the 3-prisms go together. Note that
what would be single tendons if a three-prism module was in isolation, turn
into split pairs of tendons when the modules go together. Kind of like a
<a href="https://en.wikipedia.org/wiki/Do-si-do" target="_blank">
do-si-do</a>.</p>

<img class="full-width" src="2013-03-31-calling-ball-detail.jpg">

<p>So, sooner than I could have hoped, I had my own reconstruction of Fuller's
sculpture. Just as satisfying, I had discovered the secrets of how it goes
together, and the joy of having it for my own.</p>

<img class="full-width" src="2013-03-31-calling-ball-with-shadow.jpg">

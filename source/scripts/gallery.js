// A collection of images and/or videos
// Two layouts selectable, "Presentation" or "Scroll"

ARTWORK_MAX_HEIGHT_FRACTION = 0.80;
var aPresentationVideoIsFullscreen = false;

gallery = document.getElementById("gallery");

fullscreenButton = document.getElementById("fullscreen-button");
toggleSidebarButton = document.getElementById("toggle-sidebar-button");
layoutButton = document.getElementById("layout-button");

topbar = document.getElementById("topbar");

scrollGallery = document.getElementById("scroll-gallery");
scrollGalleryArtworks = scrollGallery.getElementsByClassName("artwork")

presentationGallery = document.getElementById("presentation-gallery");
sidebar = document.getElementById("sidebar");
viewer = document.getElementById("viewer");
navigationArea = document.getElementById("navigation-area");
controlsArea = document.getElementById("controls-area");
artworkArea = document.getElementById("artwork-area");
descriptionArea = document.getElementById("description-area");


class Slideshow {
  // Show a collection of images in a slideshow format
  constructor(slides, indicator) {
    this.indicator = indicator;
    this.slides = slides;
    this.index = this.indexOf(window.location.hash.split('#')[1]) || 0
    this.slide = null;
    this.update();
  }
  update() {
    this.slide = this.slides[this.index];

    for (var i = 0; i < this.slides.length; i++) {
      this.hide(i);
    }
    this.slide["artwork"].style.display = "block";
    this.slide["title"].style.display = "block";
    this.slide["description"].style.display = "block";
    this.slide["thumbnail"].className += " active-thumbnail";
    this.indicator.innerHTML = (this.index + 1) + " of " + this.slides.length;
    descriptionArea.scrollTop = 0;  // scroll back the description area (NOTE: encapsulation breaking!)
    window.location.hash = this.slide["title"].getAttribute("name");
  }
  hide(i) {
    if (this.slide["artwork"].nodeName == "VIDEO") {
      this.slides[i]["artwork"].pause();
    }
    this.slides[i]["artwork"].style.display = "none";
    this.slides[i]["title"].style.display = "none";
    this.slides[i]["description"].style.display = "none";
    this.slides[i]["thumbnail"].className = this.slides[i]["thumbnail"].className.replace(" active-thumbnail", "");
  }
  previous() {
    this.index = (this.index - 1 + this.slides.length) % this.slides.length;
    this.update();
  }
  next() {
    this.index = (this.index + 1) % this.slides.length;
    this.update();
  }
  show(i) {
    this.index = i;
    this.update();
  }
  indexOf(titleSlug) {
    for (var i = 0; i < this.slides.length; i++) {
      if (titleSlug && this.slides[i]["title"].getAttribute("name") == titleSlug) {
        return i;
      }
    }
  }
  toggleVideoPlay() {
    if (this.slide["artwork"].nodeName == "VIDEO") {
      this.slide["artwork"].paused ? this.slide["artwork"].play() : this.slide["artwork"].pause();
    }
  }
  pause() {
    if (this.slide["artwork"].nodeName == "VIDEO") {
      this.slide["artwork"].pause();
    }
  }
  getCurrentArtworkAspect() {
    return aspect(this.slide["artwork"])
  }
}

function updatePresentationGalleryDimensions() {
  artworkAspect = eval(artworkArea.dataset["aspect"]);
  artworkMaxHeightFraction = eval(artworkArea.dataset["fraction"])

  viewerAspect = viewer.offsetWidth / (viewer.offsetHeight * artworkMaxHeightFraction);
  viewerWidth = viewer.offsetWidth;
  viewerHeight = viewer.offsetHeight;
  navigationAreaHeight = navigationArea.offsetHeight;

  // Place the artwork
  if (viewerAspect > artworkAspect) {
    // The artwork is height constrained, must preserve the bottom fraction
    artworkAreaWidth = (viewerHeight * artworkMaxHeightFraction * artworkAspect);
    artworkAreaHeight = viewerHeight * artworkMaxHeightFraction;
  } else {
    // The artwork is width constrained, allowing for the lower area to expand
    artworkAreaWidth = viewerWidth;
    artworkAreaHeight = (viewerWidth / artworkAspect);
  }
  artworkArea.style.width = artworkAreaWidth + "px";
  artworkArea.style.height = artworkAreaHeight + "px";
  artworkArea.style.left = ((viewerWidth - artworkAreaWidth) / 2) + "px"; // center laterally

  // Place the navigation area
  navigationArea.style.top = artworkAreaHeight + "px";
  navigationArea.style.width = viewerWidth + "px";
  // navigationArea.style.width = artworkAreaWidth + "px";
  // navigationArea.style.left = ((viewerWidth - artworkAreaWidth) / 2) + "px"; // center laterally

  // Place the description
  descriptionArea.style.top = (artworkAreaHeight + navigationAreaHeight) + "px";
  descriptionArea.style.height = (viewerHeight - artworkAreaHeight - navigationAreaHeight) + "px";
  descriptionArea.style.width = viewerWidth + "px";
}

function selectLayoutBasedOnWindowWidth() {
  if (window.innerWidth <= 700) {
    switchToScrollGallery();
    // Don't even allow presentation view if the window width is so narrow
    layoutButton.style.display = "none";
  } else {
    switchToPresentationGallery();
  }
}

function switchToScrollGallery() {
  scrollGallery.style.display = "block";
  presentationGallery.style.display = "none";
  gallery.style.height = "auto";
  fullscreenButton.style.display = "none";
  toggleSidebarButton.style.display = "none";
  layoutButton.innerHTML = "Presentation Layout";
  slideshow.pause();
}

function switchToPresentationGallery() {
  scrollGallery.style.display = "none";
  presentationGallery.style.display = "table";
  gallery.style.height = "650px";
  fullscreenButton.style.display = "block";
  toggleSidebarButton.style.display = "block";
  layoutButton.innerHTML = "Scroll Layout";
  updatePresentationGalleryDimensions();
  pauseAllScrollVideosExcept(null);
}

function presentationGalleryIsShowing() {
  return (scrollGallery.style.display == "none");
}

function switchLayout() {
  if (presentationGalleryIsShowing()) {
    switchToScrollGallery();
  } else {
    switchToPresentationGallery();
  }
}

function toggleSidebar() {
  if (sidebar.style.display === "none") {
    sidebar.style.display = "table-cell";
    toggleSidebarButton.innerHTML = "Hide Thumbnails"
  } else {
    sidebar.style.display = "none";
    toggleSidebarButton.innerHTML = "Show Thumbnails"
  }
  updatePresentationGalleryDimensions();
}

function onKeyDown(e) {
    if (e.keyCode == "37") { // left arrow key
      if (! aPresentationVideoIsFullscreen  ) {
        slideshow.previous();
      }
      e.preventDefault(); // don't let lateral page scroll happen
    } else if (e.keyCode == "39") { // right arrow key
      if (! aPresentationVideoIsFullscreen) {
        slideshow.next();
      }
      e.preventDefault(); // don't let lateral page scroll happen
    } else if (e.keyCode == "32") { // spacebar
      if (presentationGalleryIsShowing()) {
        slideshow.toggleVideoPlay();
        e.preventDefault(); // don't let spacebar scroll the page
      }
    }
}

function toggleFullscreen(elem) {
  if (isDocumentFullscreen()) {
    closeFullscreen();
    layoutButton.style.display = "block"
  }  else {
    openFullscreen(elem);
    layoutButton.style.display = "none"
  }
}

function addFullscreenListener(elem) {
  elem.addEventListener("fullscreenchange", function() {onFullscreen(this)});/* Standard syntax */
  elem.addEventListener("mozfullscreenchange", function() {onFullscreen(this)});/* Firefox */
  elem.addEventListener("webkitfullscreenchange", function() {onFullscreen(this)});/* Chrome, Safari and Opera */
  elem.addEventListener("msfullscreenchange", function() {onFullscreen(this)}); /* IE / Edge */
}

function pauseAllScrollVideosExcept(exceptionVideo) {
  // If `exceptionVideo` is null, then all scroll gallery videos are paused
  // Otherwise all scroll gallery videos are paused
  for (var i = 0; i < scrollGalleryArtworks.length; i++){
    artwork = scrollGalleryArtworks[i]
    if ((artwork.nodeName == "VIDEO") && (artwork != exceptionVideo)) {
      artwork.pause();
    }
  }
}

function onFullscreen(e) {
  if (e.nodeName == "VIDEO") {
    aPresentationVideoIsFullscreen = (! aPresentationVideoIsFullscreen); // hope this doesn't get discombobulated
  }
  if (isDocumentFullscreen()) {
    fullscreenButton.innerHTML = "Exit Fullscreen"
  }  else {
    fullscreenButton.innerHTML = "Fullscreen"
  }
}

function isDocumentFullscreen() {
  return (!(!document.isFullScreen && !document.fullscreenElement && !document.webkitFullscreenElement && !document.mozFullScreenElement && !document.msFullscreenElement))
}

function openFullscreen(elem) {
  if (elem.requestFullscreen) {
    elem.requestFullscreen();
  } else if (elem.mozRequestFullScreen) { /* Firefox */
    elem.mozRequestFullScreen();
  } else if (elem.webkitRequestFullscreen) { /* Chrome, Safari and Opera */
    elem.webkitRequestFullscreen();
  } else if (elem.msRequestFullscreen) { /* IE/Edge */
    elem.msRequestFullscreen();
  }
}

function closeFullscreen() {
  if (document.exitFullscreen) {
    document.exitFullscreen();
  } else if (document.mozCancelFullScreen) { /* Firefox */
    document.mozCancelFullScreen();
  } else if (document.webkitExitFullscreen) { /* Chrome, Safari and Opera */
    document.webkitExitFullscreen();
  } else if (document.msExitFullscreen) { /* IE/Edge */
    document.msExitFullscreen();
  }
}

// Button events
fullscreenButton.onclick = function() {toggleFullscreen(gallery)};
toggleSidebarButton.onclick = function() {toggleSidebar()};
layoutButton.onclick = function() {switchLayout();};

// Keyboard control
window.addEventListener("keydown", onKeyDown, false);

// Prepare for the presentation gallery - add a slide indicator
indicator = document.createElement("div");
indicator.setAttribute("id", "slide-indicator");

// Zip the artworks, descriptions and thumbnails into an array of dicts
presentationGalleryThumbnails = presentationGallery.getElementsByClassName("thumbnail");
presentationGalleryArtworks = presentationGallery.getElementsByClassName("artwork");
presentationGalleryTitles = presentationGallery.getElementsByClassName("artwork-title");
presentationGalleryDescriptions = presentationGallery.getElementsByClassName("description");
var slides = [];
for (var i = 0; i < presentationGalleryArtworks.length; i++){
   slides.push({"artwork":presentationGalleryArtworks[i],
                "title":presentationGalleryTitles[i],
                "description":presentationGalleryDescriptions[i],
                "thumbnail":presentationGalleryThumbnails[i],
              });
}

// Setup the slideshow instance
slideshow = new Slideshow(slides, indicator);

// Callback for when a document goes fullscreen
addFullscreenListener(document);

// Callback for when a video goes full screen
for (var i = 0; i < presentationGalleryArtworks.length; i++){
   addFullscreenListener(presentationGalleryArtworks[i]);
}

// Callback for when a video is played in the scroll gallery
for (var i = 0; i < scrollGalleryArtworks.length; i++){
  artwork = scrollGalleryArtworks[i]
  if (artwork.nodeName == "VIDEO") {
    artwork.onplay = function(e) {pauseAllScrollVideosExcept(e.target)}
  }
}

// Trigger initial setup at start and subsequentely as needed
switchToScrollGallery();
updatePresentationGalleryDimensions();
selectLayoutBasedOnWindowWidth();
window.onload = function() {updatePresentationGalleryDimensions(); selectLayoutBasedOnWindowWidth();}
window.onresize = function() {updatePresentationGalleryDimensions();}

// Slideshow controls
previousButton = document.createElement("button");
previousButton.setAttribute("id", "previous-button");
previousButton.innerHTML = "&larr;";
previousButton.onclick = function() {slideshow.previous()};
nextButton = document.createElement("button");
nextButton.setAttribute("id", "next-button");
nextButton.innerHTML = "&rarr;";
nextButton.onclick = function() {slideshow.next()};
controlsArea.appendChild(previousButton);
controlsArea.appendChild(indicator)
controlsArea.appendChild(nextButton);

// Use thumbnail to select slide
// see: https://stackoverflow.com/a/15860764
for (var i = 0; i < presentationGalleryThumbnails.length; i++) (function(i){
  presentationGalleryThumbnails[i].onclick = function() {
      slideshow.show(i);
  }
})(i);

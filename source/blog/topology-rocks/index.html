---
title: Topology Rocks
date: 2016-12-29
wrapping: post
---

<p>In the high frontier country of California, stands a range of rock
formations.
<sup>[<a href="#footnotes">1</a>]</sup>
It's weathered granite
forms are absolutely alluring. The whole place pulls you in with a kind of
mythical intrigue.</p>

<img class="full-width" src="2016-12-29-savina-and-mt-whitney.jpg">

<p>This is the
<a href="https://en.wikipedia.org/wiki/Alabama_Hills" target="_blank">Alabama Hills</a>,
on the outskirts of the town of Lone Pine, California.
<sup>[<a href="#footnotes">2</a>]</sup>
Savina and I have traveled here to explore the country and admire the rocks.</p>

<h2>Math on our minds</h2>

<p>The evening before, we arrived to nearby
<a href="http://www.summitpost.org/tuttle-creek-campground/892471" target="_blank">Tuttle Creek campground</a>
to spend the night. Along the way we stopped to inspect a roadside tourist map
and informational sign. We found indicated on this map a feature called the
"Mobius Arch". We had never heard of this before. Could it be related to the
famous mathematical object, the
<a href="https://en.wikipedia.org/wiki/M%C3%B6bius_strip" target="_blank">Möbius Strip</a>?
The map had a thumbnail image of the Mobius Arch to show what it looked like.
Yes, I could tell right away, from its distinct "twist" partway along the arch,
it was indeed named after the Möbius strip. What a pleasant surprise for a
Möbius enthusiast such as myself! It's lovely enough that this natural
sculpture exists out there in the form of a Möbius strip. But what is even
lovelier is the thought that there was one or more other Möbius enthusiasts out
there of sufficient influence to give this arch its mathematically inspired
name.</p>

<p>The next morning we break camp just before dawn, in order to see this place
in a magical early morning light. We head straight for the Möbius arch, and
reach it easily:</p>

<img class="full-width" src="2016-12-29-savina-jeremy-arch.jpg">

<p>In all the world, could there be a greater monument to the Möbius strip than
this? While it is actually impossible to have a Möbius strip, a two-dimensional
object, exist materially in our three-dimensional world, this mighty arch does
a fine job of representing one in the hearts and minds of math-minded humans.
To add to its prestige, as you can see, this arch forms a perfectly framed
view of the majestic
<a href="https://en.wikipedia.org/wiki/Mount_Whitney" target="_blank">Mt. Whitney</a>,
the highest summit in the contiguous United States.</p>

<p>In this moment, I can't help but think of
<a href="https://people.eecs.berkeley.edu/~sequin/" target="_blank">Carlo Séquin</a>,
the greatest Möbius enthusiast that I know. Carlo is a Möbius enthusiast to
such a degree, that he has a whole line of research and invention on the topic
of Möbius bridges. Here, you can see him explaining all about it:</p>

<figcaption>
<a href="https://www.youtube.com/watch?v=iwo7JReFTeg" target="_blank"><strong>Mobius Bridges and Buildings with Carlo Sequin</strong></a>
<br>
Video by Brady Haran for Numberphile - April 9, 2014</figcaption>

<p>I had met Carlo for the first time a few weeks ago, while attending a talk
at the <a href="http://atc.berkeley.edu/" target="_blank">Arts, Technology and Culture Colloquium</a>
at the University of California, Berkeley. I found a kindred spirit in him and
we got acquainted. I really enjoyed learning about his passions in math, art,
and computer science. So, naturally, I was contemplating all this, in the midst
of the marvelous Möbius Arch, perhaps the greatest nature-made Möbius bridge in
all the land.</p>

<h2>The difference between spheres and arches</h2>

<p>Savina and I share a picnic breakfast beside the Möbius Arch, and we observe
it carefully. But there is more to see in this place. We set out on foot to
roam around further. We walk, and look, and ponder the combination of
ruggedness and smoothness all around us. This otherworldly place has a lot to
offer. Suddenly, we come across a hill of spheres:</p>

<img class="full-width" src="2016-12-29-alabama-hills.jpg">

<p>It is
<a href="https://en.wikipedia.org/wiki/Spheroidal_weathering">spheroidal weathering</a>
of course. Ok, I didn't know it at the time, but I learned about it only after
returning home and reading about this place. How does spheroidal weathering
work? Well, it is an elaborate, four part process:</p>

<ol>
<li>A large, unbroken mass of granite is formed from magma and lies
underground.</li>
<li>Somehow (through mechanical stresses I presume) it develops a three
dimensional pattern of cracks which segment the granite into large blocks. This
is known as jointed granite.</li>
<li>Water from above ground enters these cracks and causes chemical
decomposition, starting at the surface of each block and gradually advancing
inwards. Due to the geometry of the arrangement, the decomposition happens
fastest at the corners, intermediate at the edges, and then slowest at the
faces of each block.
<sup>[<a href="#footnotes">3</a>]</sup>
</li>
<li>Sometime later, all this gets exposed at the surface, where conventional
erosion goes to work, removing the decomposed rock with ease and leaving the
rounded solid cores undamaged.</li>
</ol>

<img class="full-width" src="2017-06-03-Spheroidal-weathering.png">

<p>So that's how you end up with a hill of spheres. But look back again at the
photo above, some of these spheres have little caverns on the sides. Ah ha!
These ones seem to be on their way to becoming arches. Do arches form like the
spheres do? Do they too form underground through the same chemical decay
process? My guess would be so. And I have a hypothesis as to how:</p>

<img class="full-width" src="2017-06-03-Arch-formation.png">

<p>My hypothesis is that arches tend to form where there's a T-joint in the
granite, as illustrated above. Are you a geologist reading? If so, can you
speak to this?</p>

<p>If this isn't the case, then how else could arches form? I guess you could
simply have a cavity on one side of a sphere that starts to decay, and thus
more water enters and it decays more and more, and maybe it meets up with
another such cavity on the other side and thus you have a connected tunnel.</p>

<p>And so we return to the original question: what's the difference between
spheres and arches? Well, an arch is just a sphere with a hole through it. In
other words, an arch is a donut.</p>

<h3>Topological Safari</h3>

<figcaption>
<b>Topography:</b> the study of the arrangement of features in a
landscape.
<br>
<br>
<b>Topology:</b> the study of shape and space, related to geometry,
but somehow more abstract.
</figcaption>

<p>We continue to hike, exploring the topography of this landscape, exposed
spheres and arches all around. I still have math on my mind, and I get to
thinking about topology.</p>

<p>To a topologist, all objects are made of infinitely stretchable rubber.
Objects are categorized not by their size, nor curvature, nor volume, nor
surface area, nor any of those traditional geometric concepts. No, topological
objects can be categorized by just three particular properties:</p>

<p>1. How many borders?<br />
2. How many sides?<br />
3. Connectivity (genus)</p>

<p>This knowledge is fresh in my mind (wouldn't you know?) thanks to just a
few days ago having watched another of Carlo's videos, recently published, in
which he explains all this and more:</p>

<figcaption>
<b><a href="https://www.youtube.com/watch?v=8dHMpnfFdtc" target="_blank">Super Bottle</a>
with Carlo Sequin</b><br>
Video by Brady Haran for Numberphile - Dec 13, 2016</figcaption>

<p>Here's few topological objects to consider:</p>


<div class="partial-width-centered" style="display:table;">

	<div style="display:table-row;">
		<div style="display:table-cell;padding:0.5em;width:50%;">
			<figure>
			<img class="full-width" src="2017-06-05-disc.png">
			<figcaption>
			<b>Disc</b><br>
			one border<br>two sided<br>genus 0
			</figcaption>
			<figure>
		</div>

		<div style="display:table-cell;padding:0.5em;width:50%;">
			<figure>
			<img class="full-width" src="2017-06-05-perforated-disc.png">
			<figcaption>
			<b>Disc with one hole</b><br>
			two borders<br>two sided<br>genus 0
			</figcaption>
			<figure>
		</div>
	</div>

	<div style="display:table-row;">
		<div style="display:table-cell;padding:0.5em;width:50%;">
			<figure>
			<img class="full-width" src="2017-06-05-disc-with-two-holes.png">
			<figcaption>
			<b>Disc with two holes</b><br>
			three borders<br>two sided<br>genus 0
			</figcaption>
			<figure>
		</div>

		<div style="display:table-cell;padding:0.5em;width:50%;">
			<figure>
			<img class="full-width" src="2017-06-05-disc-with-three-holes.png">
			<figcaption>
			<b>Disc with three holes</b><br>
			four borders<br>two sided<br>genus 0
			</figcaption>
			<figure>
		</div>
	</div>

	<div style="display:table-row;">
		<div style="display:table-cell;padding:0.5em;width:50%;">
			<figure>
			<img class="full-width" src="2017-06-05-sphere.png">
			<figcaption>
			<b>Sphere</b><br>
			no borders<br>two sided<br>genus 0
			</figcaption>
			<figure>
		</div>

		<div style="display:table-cell;padding:0.5em;width:50%;">
			<figure>
			<img class="full-width" src="2017-06-05-donut.png">
			<figcaption>
			<b>Donut</b><br>
			no borders<br>two sided<br>genus 1
			</figcaption>
			<figure>
		</div>
	</div>

	<div style="display:table-row;">
		<div style="display:table-cell;padding:0.5em;width:50%;">
			<figure>
			<img class="full-width" src="2017-06-05-two-hole-donut.png">
			<figcaption>
			<b>Two hole donut</b><br>
			no borders<br>two sides<br>genus 2
			</figcaption>
			<figure>
		</div>

		<div style="display:table-cell;padding:0.5em;width:50%;">
			<figure>
			<img class="full-width" src="2017-06-05-three-hole-donut.png">
			<figcaption>
			<b>Three hole donut</b><br>
			no borders<br>two sides<br>genus 3
			</figcaption>
			<figure>
		</div>
	</div>

	<div style="display:table-row;">
		<div style="display:table-cell;padding:0.5em;width:50%;">
			<figure>
			<img class="full-width" src="2017-06-05-mobius-strip.png">
			<figcaption>
			<b>Möbius strip</b><br>
			one border<br>one side<br>genus 1
			</figcaption>
			<figure>
		</div>

		<div style="display:table-cell;padding:0.5em;width:50%;">
			<figure>
			<img class="full-width" src="2017-06-05-klein-bottle.png">
			<figcaption>
			<b>Klein bottle</b><br>
			no borders<br>one side<br>genus 1
			</figcaption>
			<figure>
		</div>
	</div>

</div>

<p>Look through each one and try and see for yourself how each of these objects
can be defined by these three properties.</p>

<ol>
<li><strong>The number of borders</strong> – This might be the easiest to
understand. From any point on an objects surface, can you travel freely without
running out of surface? On a sphere and a donut you can. On a disk you can't.
You'd come up against an edge.</li>
<li><strong>The number of sides</strong> – This is a bit trickier. Familiar
surfaces have two sides, a disk has a front and a back, a sphere has an inside
and an outside. But some special surfaces have only a single side, like the
famous Möbius strip, and Klein bottle. My understanding is that no surface can
have three or more sides. Are you a topologist reading? If so, can you speak
to this?</li>
<li><strong>The genus</strong> – this is the most perplexing, I think. Genus
tells you the number of "handles" the object has. I put "handles" in quotes,
because these are handles in the special, topological sense. Sometimes, when
trying to identify an object's topological identity, parts which seem like a
handle are not, or a part which is actually handle might not seem like one at
all. A more effective way of determining genus is this: the maximum number of
cuts through the object that you can make without the object coming apart at
all.</li>
</ol>

<div class="partial-width-centered" style="display:table">
	<div style="display:table-row;">
		<div style="display:table-cell;padding:0.5em;width:50%;">
			<figure>
			<img class="full-width" src="2017-06-05-sphere-cut.png">
			<figcaption>
				A sphere can’t even be cut through once without it coming apart. So it has
				genus 0.
			</figcaption>
			<figure>
		</div>
		<div style="display:table-cell;padding:0.5em;width:50%;">
			<figure>
			<img class="full-width" src="2017-06-05-donut-cut.png">
			<figcaption>
				A donut can be cut at most one time, without it coming
				apart. So it has genus 1.
			</figcaption>
			<figure>
		</div>
	</div>
</div>

<p><blockquote><b>EDIT:</b>Thanks to my friend Daniel for pointing out
a flaw in my original explanation. I was incorrectly applying genus to the disk
and the perforated disks. Daniel points out that the topological cuts
associated with genus need to be closed curves, as seen in the two images above
of the sphere and donut. I checked a few topological sources, and they confirm
this. Therefore, the genus of the disk, and perforated disks (of any number of
holes) is zero. Think of taking a cookie cutter to a disk, you'll end up with
the cut piece being separated. See, I told you genus was tricky!</blockquote></p>

<p>Using these three properties, you can find the true identities of objects.
You can determine if two seemingly different objects are actually the same. For
example, what about this: an object in the form of a straw?</p>

<figure style="display:block;margin-left:auto;margin-right:auto;width:200px;">
	<img class="full-width" src="2017-06-05-tube.png">
	<figcaption>
		Is this equivalent to any of those above?
	</figcaption>
</figure>

<p>How many borders does this straw have? How many sides? What genus? Once
you've got this, does it correspond to any of the above objects? If so, which
one? Then, if you imagine freely stretching and distorting it, can you convince
yourself that it is the topological equivalent?</p>

<p>Here's another puzzler, how about this object, which resembles a T-shirt?</p>

<div class="partial-width-centered" style="display:table;">
	<div style="display:table-row;">
		<div style="display:table-cell;padding:0.5em;width:50%;">
			<figure>
			<img class="full-width" src="2017-06-05-tshirt.png">
			<figcaption>
				Which of the above is this equivalent to?
				Hint: count the boundaries.
			</figcaption>
			<figure>
		</div>
		<div style="display:table-cell;padding:0.5em;width:50%;">
			<figure>
			<img class="full-width" src="2017-06-05-t-shirt-inside.png">
			<figcaption>
				Would it help to take a look from a different perspective, say bottom-up?
			</figcaption>
			<figure>
		</div>
	</div>
</div>

<p>Or try this one, which is like a vaulted ceiling with four pillars. How many
handles does it have?</p>

<div class="partial-width-centered" style="display:table;">
	<div style="display:table-row;">
		<div style="display:table-cell;padding:0.5em;width:50%;">
			<figure>
			<img class="full-width" src="2017-06-05-four-pillars.png">
			<figcaption>
				This one has no borders, to me it could be a room with four windows. How many
				handles does it have?
			</figcaption>
			<figure>
		</div>
		<div style="display:table-cell;padding:0.5em;width:50%;">
			<figure>
			<img class="full-width" src="2017-06-05-four-pillar-intermediate.png">
			<figcaption>
				What if it was stretched, like you wanted to turn it inside out but stopped
				halfway? Now can you tell what genus it has?
			</figcaption>
			<figure>
		</div>
	</div>
</div>

<p>As we walk, I think about genus. What rock formations can we find of
different genus?  We come across a monumental spheroid (genus 0):</p>

<img class="full-width" src="2016-12-29-alabama-hills-rock.jpg">

<p>Sometime later, we come across another beautiful arch, the "Eye of Alabama"
(genus 1):</p>

<img class="full-width" src="2016-12-29-alabama-hills-eye.jpg">

<p>What other topological species are out there waiting to be found?</p>

<p>Next, we come upon this intriguing specimen. It doesn't quite look like an
arch. Rather a cavern with a pillar at its entrance. Or you could see it as a
tunnel whose entrance and exit are right next to each other.</p>

<img class="full-width" src="2016-12-29-alabama-hills-savina-looking.jpg">

<p>But, of course, with a little topological know-how, you can identify it as
genus 1, therefore equivalent to a simple arch, or a donut. I am reminded of
the interconnected relationship between an arch and a tube, or between a bridge
and a tunnel. Neither can exist in isolation. Where there is one there must be
another. Like inverse pairs, like yin and yang.</p>

<p>By now I've become eager to search for higher genus formations. Could we
hope to find a double-arch (genus 2), even – dare I say it – a triple arch
(genus 3)? How rare would these be?</p>

<p>We walk and walk, and we see rock after rock. We decide we've gone far
enough. We turn back and return to our car. Despite my high hopes, we haven't
even found a single formation greater than genus-1. Too bad.</p>

<p>When suddenly:</p>

<img class="full-width" src="2016-12-29-discovering-super-chamber.jpg">

<p>Woah! This thing is off the charts. It's beyond a double arch, or a
triple arch. It's even greater than a quadruple arch (genus 4)...</p>

<p>Look at this detail, a lovely double-arch, and this is just one part of this
whole topological complex.</p>

<img class="full-width" src="2016-12-29-savina-close-look-at-chamber.jpg">

<p>Now, the question of the day: what is the genus of this outstanding rock?
Let's take a look. There's two main chambers. The first (Chamber A) is too
small to enter, but it clearly has three openings.</p>

<img class="full-width" src="2016-12-29-chamber-outside-annotated.jpg">

<p>The second (Chamber B) is cramped, but it's easy enough to crawl inside.
Here's a panoramic view from within:</p>

<img class="full-width" src="2016-12-29-chamber-inside-annotated.jpg">

<p>So we have found a formation with seven openings. Three in Chamber A and
four in Chamber B. What genus does it have? How many handles? Look at how many
places you could theoretically make a cut through without anything falling
apart. Those are the "arches", or "pillars". Chamber A has two pillars, and
Chamber B has three. So then, what we have here is a quintuple arch (genus 5)
Wow!</p>

<figcaption><b>Question: </b>what would the genus of this formation be if we
were to cut a hole that would connect the two chambers directly?</figcaption>

<p>We admire this rare quintuple arch for a while more. And then, content with
our discovery, Savina and I make our way back. It would have been enough to
find a double arch, even a triple, even a quadruple. But today nature graced
us with a quintuple arch. How about that?</p>

<p>We are nearly back to the trailhead. And, just when I thought we'd had
enough topology for one day, we find this, beside the dusty trail:</p>

<img class="full-width" src="2016-12-29-can-with-holes.png">

<p>An old can, shot full of holes for target practice. It been shot five times,
and so it has ten holes (five entry wounds and five exit wounds). What then,
is the genus of this can?</p>

<figcaption><b>More from Carlo Séquin:</b><br>
- Check out his
<a href="https://www.youtube.com/playlist?list=PLt5AfwLFPxWLfhe8-PmYYc2Gkw2YzjRWQ" target="_blank" rel="noopener noreferrer">full playlist</a>
on Numberphile<br>
- If you like topological analysis of natural formations, then you'll probably
like his paper <a href="http://people.eecs.berkeley.edu/~sequin/PAPERS/2015_Bridges_2manifolds.pdf" target="_blank" rel="noopener noreferrer">2-Manifold Sculptures</a>
on the topology of mathematical sculpture.<br>
</figcaption>

<h3 id="footnotes">Footnotes</h3>

<ol>
<li>From possible designations of urban, rural, and frontier, the United States
Census Bureau identifies this area as "frontier".</li>
<li>Why does this place have the name it does? We learn it was named after a
famous Civil War battleship the CSS Alabama, by some local prospectors in 1864
who were sympathetic to the Confederacy. To us, the name feels ill-suited. Why
should this remote and magnificent natural wonderland take its name from a
reference to this distant war, whose fighters probably had no connection
whatsoever to this place? Like it or not, you can read more about the naming of
this place
<a href="https://books.google.com/books?id=AYMPR6xAj50C&amp;lpg=PP1&amp;dq=historic%20spots%20in%20california&amp;pg=PA122#v=onepage&amp;q=alabama%20hills&amp;f=false" target="_blank">here</a>.
</li>
<li>The process of rounding off the corners and edges of a block into a
spheroid has a lovely intuition. For a process of decomposition, what needs to
happen is the water needs to penetrate into the block. Think of a bit of rock
somewhere just inside the surface of the block. If that bit of rock is just
beneath the center of one of the block faces, then water can only attack it
from one direction. If that bit of rock is just beneath an edge, then water can
attack it from two directions. And if that bit of rock is just beneath a
corner, then water can attack it from three directions. I'd like to model this
programmatically and play with simulating it. Can it be said then that the
decomposition happens twice as fast at the edges and three times as fast at the
corners? If you modeled it as a 3x, 2x, 1x process, would you end up with a
sphere?</li>
</ol>

---
title: Tensegrity Explained
date: 2013-06-06
wrapping: post
---

<p>Floating compression, also known as tensegrity, is structural magic.</p>

<img class="full-width" src="2013-03-31-calling-ball-detail.jpg">

<p>You have struts and tendons, tied together in such a way that they hold each
other up. The struts undergo compression and the tendons undergo tension. In
other words, you have sticks which push and strings which pull. Also, you get
bonus points if you build a structure such that none of the sticks or strings
are touching each other, the parts should be suspended in space. It should hold
itself up by itself.</p>

<p>These two complimentary components can be combined into such astounding
forms, it is little wonder that so much mysticism surrounds the subject.
There's the dualism of yin and yang, of push and pull, there's mutual support,
everything is in equilibrium, there's uncommon symmetries which are boldly
three dimensional. There is a special joy in imagining and contemplating and
building and playing with floating compression.</p>

<h3>Recommended reading:</h3>

<ul>
<li>For the aspiring sculptor of floating compression, Kenneth Snelson offers
	a great place to start in his lovely self-published booklet,
	<a href="http://kennethsnelson.net/Tensegrity_and_Weaving.pdf" target="_blank">Tensegrity and Weaving</a>.
</li>
<li>For a technical treatment, Hugh Kenner reveals the mechanics of these
	special structures in his book,
	<a href="http://www.ucpress.edu/book.php?isbn=9780520239319" target="_blank">Geodesic Math and How to Use It</a>.
</li>
<li>For a handy, weekend introduction, George Hart provides a
	<a href="http://www.georgehart.com/virtual-polyhedra/straw-tensegrity.html" target="_blank">tutorial</a>
	on building a spherical model with straws, paper clips and rubber bands.
</li>
<li>Finally, there's the <a href="https://tensegrity.wikispaces.com/" target="_blank">Tensegrity wikispace</a>,
	which is perhaps the most thorough compendium of tensegrity knowledge
	available.
</li>
</ul>

<h3>Rigging</h3>

<p>One of the biggest challenges in construction is getting the tendons to have
the right length and the right tension. Too short and you can't connect the
final piece. Too long and the structure is flimsy or can't hold itself up.</p>

<p>Rubber bands are very forgiving, because they're like loose springs, but
this makes them poor at holding a structure up. Twine, string, or wire is tight
and strong, because it has comparatively little stretch, but this means the
length has to be just right.</p>

<p>The ideal is to have a built in mechanism of tension adjustment, like the
knobs on a violin, or a rigging turnbuckle. But such gizmos can distract from
a bare and pure aesthetic. Kenneth Snelson uses a threaded rod inside hollow
struts to "suck" the wires inside. But for small models made with the likes of
dowels, this isn't really feasible.</p>

<p>If you can't "tune" your tensegrity post-assembly, you've got to have all
the tendons made out to the right lengths at the outset. For a regular form
this likely means all tendons of the same length. What can help a lot is a
knot-tying jig, such as nails in a board. When dealing with something low
stretch like twine, the required precision in length may be as small as 1%!</p>

<p>You may fabricate a batch of pre-tied tendons and go to assemble and find
that the effective length isn't quite what you set out for. Maybe the knot
itself tightens thus consuming less cord, and maybe also the twine "settles"
into a longer length after the initial stretch. To preserve regularity in the
final model you would then have to adjust the jig by a hair and fabricate a
whole new batch.</p>

<h4>Knot techniques</h4>

<p>Given these hurdles, my first real breakthrough came from tensegrity
builder <a href="https://tensegritywiki.com/wiki/270_struts" target="_blank">
Jim Leftwich</a>. Jim discovered that the end of a piece of nylon line can be
melted to make a stopper. Brilliant! Applying a little heat to the end is
faster, easier and perhaps more precise than tying a knot. For a spherical
tensegrity, he takes this one step further, and runs the twine through
pre-notched dowels and melts it in place, for an instant, and highly consistent
result.</p>

<img class="full-width" src="2016-06-29-strut-top-view.png">

<p>So, I adopted Jim's brilliant idea of using nylon thread and melting the
ends, and I was off to building with many fewer headaches. Melting in place,
with the twine already in the notch is the technique used in my piece
<a href="%{root}/blog/bubble-with-ninety-sticks/%{postfix}" target="_blank">
Bubble with ninety sticks</a>.</p>

<p>Notches are good when the pull direction is downwards relative to the strut
(i.e. attached on one end, being pulled towards the other). But if the pull
direction is upward relative to the strut the line would pull out. So, another
technique I've devised is using the melted end as a stopper for an overhand
knot. I'll fabricate a bunch of pieces of nylon twine and pre-melt the ends at
the desired length. Then for assembly I tie an overhand knot to hitch it to a
post, such as a nail or an eye. I use this overhand knot technique in my pieces
<a href="%{root}/blog/eight-level-dowel-tower/%{postfix}" target="_blank">
Eight level dowel tower</a>,
and
<a href="%{root}/floating-compression/calling-ball/%{postfix}" target="_blank">Calling ball</a>.</p>

<p>Later on, I realized an upward pull doesn't necessarily have to result in a
pull-out, if you layer an upward tendon underneath another downward tendon. In
other words, the tendon first in the notch wants to pull out but another tendon
on top, pulling into the notch traps it and prevents escape.</p>

<p>For structures requiring both pulling directions, this is a handy trick. It
also applies to tendons which loop over a headless post such as a finish nail
at the end of a strut. A loop would pull off the post with an upward pull,
unless it was held down underneath another loop with a downward pull.</p>

<h4>Adjustable tension</h4>

<p>With adjustability comes much more freedom. You don't have to achieve the
perfect length at the fabrication stage. You can "tune" the structure as
desired. Over the lifetime of a tensegrity, if it settles or deviates, you can
correct it.</p>

<p><a href="https://en.wikipedia.org/wiki/Turnbuckle" target="_blank">Turnbuckles</a>
really distract from the look of a tensegrity. From what I can tell, Ken
Snelson uses a custom-built turnbuckle mechanism hidden inside the ends of the
tubes. I experimented with my own amateur version of such a system, which can
be seen in my piece "Nine tight lines". But I was ultimately disappointed with
the complexity and overly mechanical feel. I wanted less machine craft, more
hand craft.</p>

<p>At the same time,
<a href="%{root}/role-models/%{postfix}" target="_blank">Jean-Pierre</a>
encouraged me to explore different materials for tensegrity, more natural, more
raw. The clear choice being bamboo, nature's ultimate struts.</p>

<p>So, I went for tension-adjustable knots. No screws, wrenches, gears or cams
required. No high precision on the fabrication length or attachment point
required. Also, knots are beautiful, they have no reason to hide.</p>

<p>The
<a href="https://en.wikipedia.org/wiki/Trucker%27s_hitch" target="_blank">trucker's hitch</a>
is a good candidate, but it it comes with a lot of knot baggage and a free tail
end which I find visually unappealing. My preferred technique is to use a pair of
<a href="https://en.wikipedia.org/wiki/Prusik" target="_blank">prusik hitches</a>
mutually attached to each other. Which I've done for the pieces, "Narcissus"
and "Aiolos" which combine bamboo and thin braided nylon cord. Braided cord
works better than twisted or laid cord with prusiks because there's no tendency
to twist when adjusting the prusik.</p>

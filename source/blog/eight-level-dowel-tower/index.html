---
title: Eight Level Dowel Tower
date: 2007-06-15
wrapping: post
---

<p>This sculpture is perplexing. It is stiff without being rigid, and it has
weight without being heavy, it has depth while appearing flat (if you squint).</p>

<figure class="partial-width-centered">
<img class="full-width" src="2008-02-18-tower-side-animated.gif">
<figcaption>
  <b>Eight level dowel tower (2007)</b><br>
    Poplar dowel rod, zinc-plated steel screw eyes, twisted nylon masonry twine.<br>
    11" x 11" x 49"
  </figcaption>
</figure>

<p>Each unit is a repeated module, like from a stamp, or from a brick wall.
  Each new layer is offset from the previous.</p>

<p>The whole thing is uniquely bouncy and springy and jiggly. In your hands, it
  is wild and alive. Maybe it can be compared to a molecule.</p>

<p>I'm proud to have been able to translate theory (see
  <a href="%{root}/blog/tensegrity-explained/%{postfix}" target="_blank">here</a>
  for some learning resources) into practice. I went through a lot of trial and
  error in fabrication and assembly before the structure came together. But when
  it did, what a delight!</p>

<p>When the components and connections are just right,  limp twine and loose
  sticks can hold together in space. For me, this piece is proof that tensegrity
  really works.</p>

<h3>Procedure</h3>
<p>I cut equal lengths of dowel and carefully pre-drilled the ends. This was a
  clumsy process with a hand-drill and bench vise. I did have to sacrifice many
  pieces where the drill hole was off-center, crooked, or the wood split.
  Though, I was able to get a rod with two satisfactory holes in the ends more
  often than not.</p>

<p>I added screw eyes to the ends. Knowing that the tendons could be pulling in
  all kinds of directions, having a versatile hitching post was important.</p>

<p>The struts complete, the next task was to find the right tendon lengths.
  Tendons play three roles in a structure like this: edge, draw or sling (as
  specified by Kenneth Snelson
  <a href="http://kennethsnelson.net/Tensegrity_and_Weaving.pdf" target="_blank" >here</a>).
  For simplicity I wanted the edge and draw tendons to be of equal length. So
  this means fabricating my tendons in two lengths.</p>

<p>Each tendon is a length of twine with each end melted to form a stopper.
  The cut end of nylon line melts easily with a candle or a lighter and
  contracts, melding into a nice strong glob (which has the added benefit of
  preventing fraying). I would cut a new piece, melt one end, then measure the
  prescribed distance, make a mark, cut again a bit beyond the mark, and then
  melt this end until the glob reaches the mark. This process yields tendons
  which are nice and uniform in length.</p>

<p>The method of hitching each tendon to each screw eye is an overhand knot,
  tied around the post, where the stopper sets the knot so it won't pull
  out. Kind of like a grappling hook.</p>

<img class="partial-width-centered" src="2008-02-18-dowel-tower-top.jpg">

<p>Then, finding the right length values is a matter of trial and error. I would
  fabricate a batch of tendons and assemble the first level. Too long and the
  rigging is slack, and the knots can come undone. Too short and the final piece
  doesn't reach, or you have to pull so hard that a strut snaps and the whole
  thing flies apart. (Safety glasses are recommended). Then I'd disassemble,
  throw that batch of tendons away and make a new batch of a new length, and try
  again.</p>

<p>It's the last tendon, the one to complete each module, which is the most
  difficult to attach. You have to pull hard and manage to the the knot with
  minimal slack. I used a
  <a href="https://en.wikipedia.org/wiki/Forceps#Locking_forceps" target="_blank" >locking forceps</a>
  to help with the pulling, and tweezers to help with the tying.</p>

<p>Soon I found the right length values and the structure came together just
  right: tight and stiff, but not so much so that it was impossible to
  assemble.</p>

<img class="full-width" src="2008-02-18-dowel-tower-close.jpg">

<p>In his reference (linked above) Kenneth Snelson specifies six draw tendons
  per level. But I found you can get away with only three, and this makes the
  sculpture much more bouncy and lively. I think the way it moves is the best
  part.</p>

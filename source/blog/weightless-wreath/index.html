---
title: Weightless Wreath
date: 2016-12-08
wrapping: post
---

<p>This sculpture is a modern take on a traditional ornament. It is both
familiar and unfamiliar at the same time.</p>

<video class="full-width" autoplay loop src="2016-12-08-weightless-wreath-spinning.mp4"></video>

<p>It can be called a wreath in the sense that it is a circular arrangement of
plant matter made for aesthetic purposes. It can also be called a
<a href="%{root}/blog/tensegrity-explained/%{postfix}" target="_blank">tensegrity</a>,
or floating compression, in the sense that it is a mathematical arrangement of
sticks and strings which holds itself together using a counterbalancing network
of tension and compression.</p>

<p>It is lovely to think about. It is lovely to look at. It can stimulate
multiple parts of our minds all at once.</p>

<p>I recommend hanging it vertically on a wall or door, at eye level, so that
when you face it you can look into its empty center. Just as with jazz, it is
the empty spaces between the notes which make the music great, not just the
notes themselves. As you can see, this sculpture is full of jazz.</p>

<img class="full-width" src="2016-12-08-weightless-wreath-top.jpg">

<figcaption>
<strong>Weightless Wreath (2016)</strong><br>
8 x 8 x 4 inches<br>
Bamboo twigs and hemp twine
</figcaption>

<p>I built it with love, and assistance from Savina. It is composed of natural
materials.</p>

<p>To begin, I selected seven twigs to use for the struts, cut them to length,
and then found fourteen smaller twigs which would fit into the ends of the main
twigs, to form studs for holding the string ends in place. Each stud was chosen
carefully for a precise fit into the end of each strut. A whole bag of bamboo
twigs of varying thicknesses made this job pretty easy. Bamboo occurs naturally
with an infinite variety of thicknesses, and each portion has a slight taper as
well, so you can mate pieces quite snugly, like sealing a wine bottle with a
cork.</p>

<img class="full-width" src="2016-12-08-weightless-wreath-1.jpg">

<p>Then, I tied 14 equal lengths of string with fixed loops on the ends, using
the overhand knot on a bight. These will form the fixed tendons on the two rims
of the cylindrical structure. I pretty much guessed at the length, and it
turned out I was lucky in the end. What I did was measured the seven distances
between strut ends with the sticks arranged as in the above photo, and took
their average.</p>

<img class="full-width" src="2016-12-08-weightless-wreath-2.jpg">

<p>Then, assembly begins. I knew that each fixed tendon would have a tendency
to fall off during the "raising" of this sculpture, so I added tape flags to
the end of each stud to contain them.</p>

<img class="full-width" src="2016-12-08-weightless-wreath-3.jpg">

<p>Then, with four hands (two from Savina and two from me) we dexterously add
seven rubber bands as temporary edge tendons, and the sculpture comes to life.
It took us a few tries to get this assembled and stable.</p>

<img class="full-width" src="2016-12-08-weightless-wreath-4.jpg">

<p>Then one by one, each rubber band is replaced by an adjustable hemp tendon.
This is built in two parts: (1) A lower portion which is a length with fixed
loops on each end, overhands on a bight again. (2) An upper portion which is a
longer length with only one fixed loop, overhand on a bight, and one free end.
The free end meets one of the fixed loops in the middle and can be wielded like
a pulley, and then stopped off with an overhand slip knot. Here is a closeup
with one of the adjustable tendons highlighted:</p>

<img class="full-width" src="2016-12-08-weightless-wreath-5.jpg">

<p>Then I go through and tune the tensegrity by adjusting the tension on each.
I want it to be tight, but not too tight. I also want each of the top and
bottom faces of the cylinder to be close to parallel, in other words, not a
lop-sided cylinder.</p>

<p>Once I'm happy with the tension all around, I lock it in by pulling through
the free end of each overhand slip knot. Now each overhand slip knot has become
a plain overhand knot, and thus fixed for good. The tension in this sculpture
is no longer adjustable. Only time, and changes in the weather, will adjust its
tension from here on out.</p>

<p>Finally I trim the free ends for each locked in overhand knot, and
construction is done.</p>

<img class="full-width" src="2016-12-08-weightless-wreath-side.jpg">

<p>The artist
<a href="%{root}/role-models/%{postfix}" target="_blank">Kenneth Snelson</a>
made a career for himself building tensegrities large and small with space-age
steel and aluminum. But here, you have one built with humble plant matter,
which I find gives a special beauty and appeal. Snelson's are monumental, this
one is artisanal.</p>

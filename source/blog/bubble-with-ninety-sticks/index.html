---
title: Bubble With Ninety Sticks
date: 2007-02-17
wrapping: post
---

<p>Who knew you could make a bubble with sticks and string?</p>

<figure class="full-width">
<img src="2008-02-18-bubble-with-sixty-sticks-pentagon.jpg">
<figcaption><strong>Bubble with sixty sticks (2007)</strong></br>
Poplar dowel rod, twisted masonry twine. 31" diameter</figcaption>
</figure>

<p>Bubbles, (imagine a typical soap film bubble) are round and hollow, a
membrane enclosing a pocket of air. Sticks are thin and straight and solid.
And strings are filaments, not membranes. But with the magic of tensegrity,
these two elements, combined, can take on surprisingly bubble-like properties.</p>

<h3>Push-pull analysis:</h3>

<p>How? Each stick gets its own string, attached to a notch in each end, like
an archer's bow. And these "bows" can be joined to other bows by the same
notches. If you join bows in a repeating polygonal formation, like weaving
or net making, you can get a sheet of bows. This is based on the idea of a
<a href="https://en.wikipedia.org/wiki/Euclidean_tilings_by_convex_regular_polygons" target="_blank">
mathematical tiling</a>. And if you make the polygons just right, you can get
a <a href="https://en.wikipedia.org/wiki/Spherical_polyhedron" target="_blank">
spherical tiling</a>, that is, where the sheet curves and closes in on itself.</p>

<p>So the sticks and strings make up a curved sheet enclosing empty space. It
is bubble-like already. But what about surface tension? A bubble exists because
the surface tension of the film wants to contract inwards. But the air inside
resists this contraction. The air pushes back against the pull of the film.
This push and pull balance each other out. With tensegrity the same thing
occurs, push and pull oppose each other in equilibrium. Only this time, the
strings are doing the pulling and the sticks are doing the pushing.</p>

<p>Unlike a soap bubble, there's no contraction of a film against a resisting
volume of air. Instead, the resistance in this sculpture exists in the sheet
itself. Remember, the strings are fixed on each bow, and so the sheet resists
being bent or curved, because this stretches the strings. In other words, the
sheet wants to be flat. The key is to have just enough stretchiness in the
strings that the sheet can close into a sphere. (But not too much stretchiness
or the whole thing will end up limp)</p>

<p>When it can close, then there's no further hope for it to be a flat sheet.
Each local region tries to be flat but it can't because the neighboring
regions are pulling down on it, forcing it to be curved, and this is happening
all around. It's a hopeless situation, everyone is keeping everyone else in
check, a total impasse.</p>

<h3>Construction:</h3>

<p>The particular spherical tiling in this sheet of sticks and strings is one
of my favorites. the <a href="https://en.wikipedia.org/wiki/Truncated_icosahedron" target="_blank">
truncated icosahedron</a>, consisting of twenty hexagons, and twelve pentagons,
a shape made famous by the classic soccer ball.</p>

<p>The truncated icosahedron has ninety edges, and so this sculpture has ninety
sticks. It has sixty vertices, and and so too does this sculpture. But each
vertex is not a point, but rather a twisted triangle. Imagine three hands
cyclically holding each other by the wrist. Kind of like this:</p>

<img class="partial-width-centered" src="2016-06-28-three-hands.png" width="300" height="239">

<p>This type of hand holding is a natural way to join the bows to each other.
Instead of each hand grasping the next wrist, you have each notch grabbing the
next string. This arrangement means there's a certain twistedness in the
geometry of the sculpture overall. This is closely related to the mathematical
concept of <a href="https://en.wikipedia.org/wiki/Chirality_(mathematics)" target="_blank">
chirality</a>.</p>

<img class="full-width" src="2016-06-29-vertex.png" width="660" height="560">

<p>The twine which makes up the "bowstring" spans between the two notches, but
unlike an archer's bow, the string passes diagonally across and connects to the
opposite side. This pass across from one side of the stick to around to the
other imparts a certain twistedness to each bow. Chirality is embodied at
multiple levels in this sculpture.</p>

<img class="partial-width-centered" src="2016-06-29-strut-oblique-view.png" width="300" height="224">

<p>The pieces of twine are secured by melting the nylon to form a glob which
acts as a stopper. You can read more about this technique (invented by
Jim Leftwich as far as I know), <a href="%{root}/blog/tensegrity-explained/%{postfix}" target="_blank">
here</a>.</p>

<img class="full-width" src="2008-02-18-bubble-with-sixty-sticks-detail.jpg"></a>

<p>The advantage of bringing the string around from one side to the other is
that the notches of all sticks remain in the same plane, that is, tangential to
the sphere. Sticks can connect to the strings of their neighbor sticks without
requiring a roll of quarter turn to align the notches. All the notches line up
naturally. This is essential for connecting bows together to form a sheet.</p>

<img class="full-width" src="2008-02-18-bubble-with-sixty-sticks-hexagon.jpg" width="660" height="498"></a>

<h3>Behavior</h3>

<p>The bubble is springy and bouncy and lively. You can roll it, toss it, and
squish it (within limits). And none of the sticks are touching each other. It
makes a lovely clattering sound when it rolls on a hard surface. There is an
appreciable amount of stored energy in the work. Specifically, elastic
potential energy. If you take a pair of scissors to one of the strings,
the bubble <em>will</em> rupture.</p>

---
title: Journey In The Tufa Kingdom
date: 2016-12-27
wrapping: post
---

<p>In a distant land, across mountains and deserts and upon a high plain, lies
a lake of great peculiarity.</p>

<img class="full-width" src="2016-12-27-savina-jeremy-mono-lake.jpg">

<p>Water flows in, but no water flows out. The only
way water leaves is by evaporation. And when the water evaporates, it leaves
salt and other goodies behind. So the lake is salty, and alkaline, and rich in
carbonates. What’s more, there are subterranean springs which bring
calcium-rich water. When this spring water meets the lake water, presto! You
have a reaction, and calcium carbonate, a.k.a limestone is precipitated. Get a
spring like this going for many decades or centuries and amazing limestone
towers known as tufas will form. Savina and I journeyed to see these famous
tufas at Mono Lake, and here is what happened:</p>

<figcaption>
<b>Tuesday December 27, 2016</b><br>
Owens Valley, California.<br>
Clear skies with wisps of cirrus.<br>
Temperature 32˚F, 0˚C.<br>
Elevation 6400ft, 2000m.</figcaption>

<p>We’re driving south on highway 395. Finally, after many years, today’s the
day we go to visit Mono Lake. A sign reads “South Tufa 5mi”. So we continue
south 5+ miles, then realize our mistake. We backtrack north to the same sign.
Turn off the highway onto a side road, headed east bound. The lake is now
correctly 5mi ahead. The road approaching is icy, but only on the westbound
side. All around, we see bright red rusty sagebrush mixed with faded golden
grass.</p>

<p>We then turn onto a loose gravel road, but we don’t know this yet because
the initial section is covered in snow. Do we drive over the snowed road to the
visitor station? Or is it too slippery? Another two-wheel-drive car comes and
we wait. We watch to see how it goes. Ok, no sliding off, not too slippery.</p>

<p>We arrive. No need to pay, our interagency annual pass takes care of us. We
are a half mile from the water’s edge. Sun at our backs, such convenient
lighting. We hike forwards, not yet realizing how impressive this place will
turn out to be.</p>

<p>Tufas. Tufas everywhere. The smallest are many millimeters, the tallest are
many meters. Strange towers of frozen mineral movement. A first impression: it
reminds us of the
<a href="https://xanxano.com/2016/10/17/pla_de_tudela/" target="_blank">Pla de Tudela</a>,
in Catalonia, where Salvador Dalí too spent time among strange and beautiful
rock formations.</p>

<p>We have entered a wild and majestic sculpture gallery. Savina is sitting by
the water’s edge. There is a tufa island just offshore. I setup my phone for a
self-timer portrait, but it dies of hypothermia. I shouldn't have tried to
balance it on this rock, which promptly conducted all the heat out of it.
Sometimes beauty and danger go hand in hand.</p>

<p>I sit with Savina, we taste salt crust on the rocks at the water’s edge.
Close by, green algae permeates the shallow fringes. Interesting ripples coming
up onto the broad, gently sloped surface of a large salted rock, some sort of
miniature erosion cycles going on. Little flows of lake water dissolve micro
channels in the salt, which in turn guides the water into more interesting
ripples which in turn further carve interesting micro channels, and so on and
so on, without end.</p>

<p>A tranquil timespan passes. Then, a bright red helicopter approaches behind
our backs, appears, makes a fast and low pass alongside the tufa island, then
just as quickly returns from where it came. Heli-tourists, we guess. Next, a
flock of birds picks up and does a lap around the tufa island, returning to
where they were before. Other birds from here, we learn, journey all the way to
Argentina and back.</p>

<p>Duck-like birds floating out there dive under with a <em>thwop</em>, to
emerge in an unpredictable place some time later. Savina says we can know these
birds eat mud because of the shape of their beaks. “Would you like to have some
mud for lunch?”, I ask. “No, because I don’t have one of those beaks”, she
responds.</p>

<p>We walk along the shore with the tufa island out there. Should we swim to
it? Too cold still, the morning low lingers. Even if it were not so, there is
still the green algae cloud all along the bank, and most of all, it would feel
plain strange to swim in such a surreal place. We notice frost, soft and
pillowy like snow, coating the coarse sand by the water’s edge. Is it salty?
Yes, we taste. Never had salty snow before.</p>

<p>My phone revives in the warmth of my jacket pocket. I can take more
pictures. The intense sun makes our eyes tired. To the east is the Great Basin
of North America, with no fences, not the remotest sign of civilization to be
seen at all. To the west are the enormous Sierra Nevada mountains. They are far
beyond this little world. Can such mountains understand the life of a tufa?</p>

<img class="full-width" src="2016-12-27-mono-lake-2.jpg">

<p>Then we turn inland and continue walking among the towers of mineral. The
tufas hold onto a coat of snow on their shady north faces. The land all around
is rugged, but these tufas are a special kind of rugged. They are turbulent and
voluminous with bubbling and corroded faces. They reach straight up, more or
less, not as straight as the conifer trees far off in the distance. Some are
leaning, some are melded together, some are holding hands.</p>

<p>We walk and look and stop and look and all is extraordinary. We are in a
perpetual state of distraction. We are following our feelings as they fly
around the tufa tops. We step out into a clearing where we encounter some other
tourists. They have emerged from a hide and go seek wonderland. “Hello”, I say,
but the hello doesn’t stick. We are all in a transfixed state.</p>

<p>Once more, we sit by the water’s edge, gently lapping, enough breeze to make
the mirror versions of the lake tufas animate and stretch and distort and break
apart. We are in a Dalí painting, especially if you squint. If he traveled
here, halfway around the world from his home town, would he find this place
strange or would he feel right at home?</p>

<p>Savina and I walk further into tufadom. I thought it would be only this one
cluster, but we find more and more. All is still. The beach is covered in ice.
It cracks under our feet. Savina finds air bubbles trapped underneath and
causes them to hurriedly travel on wild paths to reach a gap and reunite with
our atmosphere. At one point I crack through the ice, and my boot plunges into
the mud below. The tufas react with indifference.</p>

<img class="full-width" src="2016-12-27-jeremy-mono-lake-3.jpg">

<p>We learn a few important facts, thanks to the interpretive signs. No water
flows out of this lake, it only leaves by evaporation. The native people ate
these little sand flies, a delicacy. Fly-meal was exported to other tribes near
and far. There is a peculiar species of brine shrimp that lives here. Each
generation lasts only one season. They hatch in the spring, grow up and enjoy a
prosperous brine shrimp life throughout the summer, lay their eggs in the fall,
and die shortly thereafter. Then the eggs lie dormant over the winter, awaiting
their own turn in the new year. What a fate. Imagine, the entire population of
your species in lock step in the same lifecycle stage as you. Nobody to nurture
you when you're young or to be nurtured by you when you're old. Nobody to help
you ever see where you come from or where you're going.</p>

<img class="full-width" src="2016-12-27-savina-mono-lake-4.jpg">

<p>Later on, we find some dynamic activity at a disorganized part of the
shoreline: water flowing in various directions. A spring! We study the
minuscule flows of water and find a spot from where it emerges, a fist-sized
hole where clear water with small pebbles flows out. A source of pure divergence,
the matter radiating out in all directions. I step closer and my foot sinks a
few inches. This causes the spout to erupt with sediment. The spring is cloudy
and debris from the subsurface tunnels that I have collapsed, is carried out.
Will it clear up? We wait a few minutes and it resumes its original state. All
my destruction is washed away.</p>

<p>Some decades ago there was another collapse here, when the city of Los
Angeles diverted water from nearby tributaries – water which would have
otherwise flowed into this lake. The lake level fell. An island out in the
middle became no longer an island, but a peninsula. A new land bridge emerged.
Predators could now reach birds which were previously out of reach. Birds which
nested on this island could do so no longer. This was many decades ago, and
since then the water level has returned. But the birds have not entirely. If a
new generation of birds nests where its parents nested, then a single
displacement event can dramatically change the course of bird history. Another
way to look at this is that the Bird population “remembers” the trauma of the
land bridge invasion, and is still too traumatized to return. I learned this
story from Savina some time later, in another tufa cluster. She had read it on
one of the interpretive signs.</p>

<figcaption>Read more about the birds of Mono Lake and the story of the
California Gull and the land bridge to Negit island
<a href="http://www.monolake.org/about/ecobirds" target="_blank" rel="noopener">here</a>.
</figcaption>

<p>Now, the sun is on its way down in the west. We are returning to the station,
but through a different path. We walk through the grass and brush, on the
inland side of the tufas. The trails we follow back support lots of animal
traffic. Yet, we have come at a time apart from local rush hour. So we see no
animals. But we clearly see their movements. How is this? Their tracks in the
snow are everywhere. Hares, mice, deer, coyote maybe? They are all crossing in
a labyrinth of sweet and sour sage brush.</p>

<img class="full-width" src="2016-12-27-mono-lake-5.jpg">

<p>Soon we are back to the station and my mind is as big as the sky. While we
picnic the sun warms up our backs. As we eat our lunch, I look over at Savina
and she appears so contented I almost thought it was her birthday. We say good
bye to Mono Lake. Now we may continue our trip, heading further south through
the Owens Valley. Savina drives and I write some more. The afternoon sun is
low and in our faces. My pen casts a long shadow on the page.</p>

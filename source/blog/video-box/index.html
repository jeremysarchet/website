---
title: Video Box
date: 2019-12-19
wrapping: post
---

<p>I've built a digital video artwork display. The idea is to have a monitor
and a media playing device (a Raspberry Pi) together inside an attractive box
with a single power cord and a single power switch.</p>

<img class="partial-width-centered" src="2020-01-11-angled-portrait.jpg">

<p>When you flip the switch, the monitor and device both power on and the
device plays one or more videos in fullscreen endlessly on loop.</p>

<h3>Building the box</h3>

<p>I designed the box around an extra monitor that I had. It's a construction
similar to my
<a target="_blank" href="%{root}/blog/frame-making/%{postfix}">handmade picture frames</a>
but with deeper proportions and with dowels to strengthen the corner joints.
The monitor's bezel isn't uniform on all sides, so I made the box to accommodate
and centered the monitor inside with shims and made a pair of foam-lined blocks
to hold it snugly in place at the top and bottom. Across the rear-inside-top is
a strip of wood which is used to support the box via a cleat mounted to the
wall.</p>

<img class="full-width" src="2019-12-19-back-portrait.jpg">

<p>The rigging inside is made simple thanks to ordering off-the-shelf cables
and power cords. No soldering or splicing necessary. The power switch is the
simple plug-in kind mounted to the side of the box. From the wall outlet, an
extension cord brings power to the plug-in switch. After the switch there's a
power chord splitter, and one branch goes to the monitor and the other to the
Pi. Finally, the Pi connects to the monitor via HDMI.</p>

<p>I was a bit concerned about the monitor and computer being mostly enclosed
and overheating. I thought I'd try out a fan. I was thinking to myself, it
couldn't hurt to have a little breeze in there. (There's a ~5mm gap between
the box and the wall thanks to felt pads in the corners so it's not completely
sealed). I was thinking "how noticeable could it be?" But when I tried it
out with the fan, I couldn't stand it. The noise was faint but unignorable. So
I disconnected the fan and it seems to work fine.</p>

<h3>Using a Raspberry PI</h3>

<p>There's a number of popular ways to setup a Raspberry Pi as a dedicated video
player, and it seems to be fairly common in the world of digital and media art,
which is just my context.</p>

<p>The Raspbian Operating System (a distribution of Linux) comes with a nice
minimalist command line video player called "Hello Video" that plays a given
video in loop. And playback is perfectly seamless, there's no gap, pause or
stutter when it loops around, which I love. Many of my video artworks are
created to be viewed as loops. Hello Video only takes videos in "raw" h.264
format, which I had never heard of before, but I suspect enables the seamless
looping. Perhaps performance is better too.</p>

<p>As for setting up the Pi to play a given video with "hello_video" on boot
I've been using a simple codebase of shell scripts which I've modified slightly
for my needs. There's also instructions for using ffmpeg to convert a video to
"raw" h.264. You can find the code and learn about setting it up here:</p>

<figcaption>
<a target="_blank" href="https://www.gitlab.com/jeremysarchet/art-booter">www.gitlab.com/jeremysarchet/art-booter</a>
</figcaption>

<h3>Other options considered</h3>

<p>In my research I found a handful of options for standalone "media players"
which play videos from either a USB drive or internal storage. But one gets the
impression they aren't quite designed with "play video on boot endlessly" in
mind. They seem to need user intervention such as navigating a menu and pressing
play. Along similar lines, there are industrial grade "digital signage" devices
which do seem to have this in mind, but they feel overkill for my context and
from what I could find, entry level models start at around $400.</p>

<p>There are also numerous devices designed as "Digital Picture Frames", which
however don't inspire much confidence in the ease with which you could
set one up to be single-purpose to play a video in loop upon power on.</p>

<p>The last option I looked into were "Fine Art Digital Canvases", which
appear to be in fashion, with great looking hardware and modern design,
and often a high price-tag. Unfortunately, as far as I can tell, the business
model is all about subscription, like Spotify or Netflix, and the displays
themselves are all "smart" devices and require an internet connection and
account. They also seem geared towards still images. What I wanted was simply a
"dumb" device, but I suspect that there's not much of a market for a dumb fine
art digital canvas.</p>

<p>I came away skeptical that I could find an off-the-shelf device that would be
as suitable for my principles as a dumb display plus a small single board
computer (a Raspberry Pi) in a handmade box hooked up to a single switch.</p>

<h3>Conclusions</h3>

<p>I'm excited at the prospect of being able to produce my own physical video
packages. It's satisfying to have a physical thing with a single switch that
when you power it on, it starts playing a video in loop, and when you're done
you power it off. I see it as the pinnacle of user/gallery friendliness. No
need to setup an account, connect to internet, deal with video files, etc.
Just plug in and power on. And when you're done, just power off.</p>

<img class="full-width" src="2020-01-11-portrait.jpg">

<p>I find it most appealing to be able to offer a "dumb" device, and I can even
foresee selling video-box artworks like this to individual collectors.</p>

<p>I think there's something special that happens when my
<a target="_blank" href="%{root}/deanimation/%{postfix}">deanimation videos</a>
are presented like this. Watching the videos on a laptop or computer feels too
unceremonial. With a video-box it feels more elegant and dignified and
contemplative.</p>

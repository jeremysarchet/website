---
title: Camera Comparisons
date: 2019-05-22
wrapping: showcase
---

<p>This interactive plot lays out the resolution and frame rate capabilities of
leading digital cameras. You can mouseover or tap on any of the camera names
to highlight it in the plot and view stats. You can also select and filter
cameras from the plot with the buttons and checkboxes.</p>

<h3>Background</h3>

<p>My work deals with image sequences, whether they are are taken from the
frames of a video or from continuous photo shooting (burst mode), or from
interval shooting. The subjects I capture move at a variety of speeds. Some,
like a hummingbird in flight, would benefit from 60 fps. And others, like
floating leaves slowly drifting on a pond, can be captured as photos at one
second intervals (1 fps).</p>

<p>So when looking at cameras, one question I had was: is there a camera that
is well-rounded and can perform well across this whole range? Or would I be
better off getting one photo-specific camera for slow moving subjects, and a
separate video-specific camera for fast moving subjects? My price range was
from $1-5k.</p>

<p>One criteria that I started with was the ability to record video at
4k/60fps. There's a handful of consumer-grade cameras I've found that can do
so. But I also want to be able to capture subjects at 30fps, and ideally a
camera that can do 4k at 60fps can do even higher resolution at 30fps, and then
again even higher resolution at 10fps, and so on.</p>

<h3>Apples to apples</h3>

<p>For instance, 4k video is 4096 × 2160 = 8,847,360 pixels, or 8.8 megapixels.
And perhaps a camera that can do 4k/60 can capture even more pixels at 30fps?
Say, 5k-6k? or their burst-mode equivalents? And I'd like it to be able to take
even higher resolution photos for interval shooting. Where this plot helps, is
that it compares these settings uniformly, by treating everything in terms of
megapixels.</p>

<p>In principal, given a certain data capture rate, you could trade off frame
rate for resolution arbitrarily. For example, all else being equal, if you
could set your camera to capture exposures of, say 1000x1000 pixels
(1 megapixel) at 40 frames per second, then you could equally well set it to
capture exposures of 2000x2000 pixels (4 megapixels) at 10 frames per second.
Both involve the same amount of pixel-processing per second. In practice
however, with consumer cameras you only have the preset resolution and frame
rate settings to work with. You can't just dial these values up or down as
you wish. (Though I believe there are certain classes of high-end cameras out
there where you can do so.)</p>

<h3>Few cameras try to do it all</h3>

<p>As you look at the chart, notice how cameras tend to cluster together on
one end of the spectrum or another. And notice how the cameras with full frame
sensors push higher into the megapixel count, and the cameras with micro four
thirds sensors push higher into the frame rate count. Also notice that 4k/60fps
video is widely supported, yet >4k/30fps video is not as much. Perhaps it's not
as in demand?</p>

<h3>My camera of choice</h3>

<p>After plotting the data like this, and flipping through the different
curves, one camera stood out: the Fujifilm X-T3. It was a kind of a blind
audition. I had initialy discounted this camera as being not the right choice
for me, for more subjective reasons. (I hadn't an impression of Fujifilm as a
leader in digital cameras, and this model looked like it was designed for retro
aficionados – perhaps it merely offered style at the expense of performance).
But, based on my reading of the data, the X-T3 is the most well-rounded of the
bunch.</p>

<h3>Sensor size</h3>

<p>Regarding sensor size, it's a clear bonus that the X-T3 has the larger
APS-C size sensor. Also, as far as I know it's the only consumer-class APS-C
camera that can do 4k/60fps video. All the others that can do 4k/60fps use
micro four thirds sensors. That is, until the Sony a7s III comes out, which
will be the first full frame camera of this class able to do 4k/60fps. But
one question I have is, will it be able to do greater than 4k at 30fps? I'm
not so sure that it will. I suspect it's not as in demand.</p>

<p>Going from APS-C to a full frame system appears roughly double in price.
That feels fair, especially considering that the sensor-area is roughly double
too.</p>

<ul>
  <li>Full frame sensor area: 36 x 24 = 864 mm<sup>2</sup></li>
  <li>Fuji APS-C sensor area: 23.6 x 15.7 = 370 mm<sup>2</sup></li>
  <li>Four thirds sensor area: 17.3 x 13 = 225 mm<sup>2</sup></li>
</ul>

<p>For a Sony Alpha body and a pair of good lenses covering a wide range of
focal lengths, I estimate the price at about $6k. For the Fujifilm X-T3 body
and a pair of good lenses covering a wide range of focal lengths, I estimate
the price at about $3k. After considering the benefits of going full frame
against the cost, and taking into account that I'm not really working in
low-light conditions (where full frame is a clear win) and taking into account
that the a7s III isn't yet released, and after reading a lot about Fujifilm's
cameras and lenses, I confirmed my decision. For my current work I'll go with
the X-T3.</p>

<h3>In the context of my work</h3>

<p>I produced my original batch of motion photos/videos using my trusty digital
camera from 2009. It could capture 1280x720/60fps and 1920x1080/30fps. These
resolutions are quite low for making still images and prints. It was a clear
limiting factor, and one of the reasons I saw my initial work as essentially a
proof of concept.</p>

<p>Now that the concept has been proven, it's time to invest in a better tool.
I've done some careful research on currently available cameras. For my work,
I'm interested in moving subjects of a variety of speeds, and I'd like to have
a setup (with one camera or maybe two cameras) that can perform well across a
wide spectrum of frame rates.</p>

<h3>How this plot was conceived</h3>

<p>When researching cameras it became apparent that I ought to consolidate all
the resolution and frame rate specifications for the leading cameras, and so
at first I did so in a spreadsheet. Then I decided I'd like to make a plot, and
then I liked that plot so much, I decided to make a better plot, and make it
interactive, and use it as an exercise for my javascript and data visualization
programming skills.</p>

// 2019-05-16
// Plot comparing camera capture envelopes for sequential image capture
// Comparing apples to apples between burst mode and video modes

// Data for all the cameras in question
const cameras = [
{name: 'Sony Alpha a9',
  data: [ [24.2, 20, 'up to 362 frames'], [8.3, 30], [2, 120] ],
  price: '$3.5k',
  sensor: 'Full frame',
},
{name: "Sony Alpha a99",
  data: [ [42, 12], [8.3, 30], [2, 120] ],
  price: '$3.2k',
  sensor: 'Full frame',
},
{name: "Sony Alpha a7s III (TBD)",
  data: [ [12.2, 5], [8.8, 60], [2, 120] ],
  price: '$3.0k',
  sensor: 'Full frame',
},
{name: "Sony Alpha a7r III",
  data: [ [42.4, 10, 'up to 76 frames'], [8.3, 30], [2, 120] ],
  price: '$2.8k',
  sensor: 'Full frame',
},
{name: "Panasonic Lumix GH5S",
  data: [ [10.3, 12, 'up to 600 frames'], [8.8, 60], [2, 240] ],
  price: '$2.2k',
  sensor: 'Micro 4/3',
},
{name: "Sony Alpha a7s II",
  data: [ [12.2, 5, 'up to 200 frames'], [8.3, 30], [2, 120] ],
  price: '$2.0k',
  sensor: 'Full frame',
},
{name: "Sony Alpha a7 III",
  data: [ [24.2, 10, 'up to 177 frames'], [8.3, 30], [2, 120] ],
  price: '$2.0k',
  sensor: 'Full frame',
},
{name: "Fujifilm X-T3",
  data: [ [26.1, 11, 'up to 145 frames'], [16.6, 30, 'up to 60 frames, or more if lower fps'], [8.8, 60], [2, 120] ],
  price: '$1.5k',
  sensor: 'APS-C',
},
{name: "Panasonic Lumix DC-GH5",
  data: [ [20.3, 12], [18, 30], [8.8, 60], [2, 180] ],
  price: '$1.5k',
  sensor: 'Micro 4/3',
},
{name: "Blackmagic BMPCC 4k",
  data: [ [8.8, 60], [2, 120] ],
  price: '$1.3k',
  sensor: 'Micro 4/3',
},
{name: "Panasonic Lumix DC-G9",
  data: [ [20.3, 20], [18, 30], [8.3, 60], [2, 180] ],
  price: '$1.2k',
  sensor: 'Micro 4/3',
},
];


// Color palette from https://sashat.me/2017/01/11/list-of-20-simple-distinct-colors/
const palette = [
[230, 25, 75],
[60, 180, 75],
[255, 225, 25],
[0, 130, 200],
[245, 130, 48],
[145, 30, 180],
[70, 240, 240],
[240, 50, 230],
[210, 245, 60],
[250, 190, 190],
[0, 128, 128],
[230, 190, 255],
[170, 110, 40],
[255, 250, 200],
[128, 0, 0],
[170, 255, 195],
[128, 128, 0],
[255, 215, 180],
[0, 0, 128]
];


function addCheckboxAndLabel(parent, className, labelMsg) {
  var checkbox = document.createElement('input');
  checkbox.type = 'checkbox';
  checkbox.id = labelMsg;
  checkbox.className = className

  var label = document.createElement('label')
  label.appendChild(document.createTextNode(labelMsg));

  parent.appendChild(checkbox);
  parent.appendChild(label);

  return checkbox
}

function addButton(parent, id, className, onclick, text) {
	var button = document.createElement('button');
	button.type = 'button';
	button.id = id;
	button.class = className;
	button.onclick = onclick;
	button.innerHTML = text;
	parent.appendChild(button);
	return button;
}

function addElement(parent, type, attributes = {}, textContent = '') {
	// Helper function, takes a string `type` and a dict `attributes`
	// and appends the new element to the SVG and returns it
	var element = document.createElementNS(ns, type);
	parent.appendChild(element);
	setAttributes(element, attributes, textContent);
	return element;
}

function setAttributes(element, attributes = {}, textContent = '') {
	// Helper function, takes a DOM element `element` and a string
	//`textContent` and a dict `attributes` and applies them all
	// to the element
	element.textContent = textContent;

	for(var key in attributes) {
		element.setAttribute(key, attributes[key]);
	}
}


// Setup
const ns = 'http://www.w3.org/2000/svg';
const svg = document.getElementById('my-svg');

// Sizing
const svgWidth = 550;
const svgHeight = 450;
setAttributes(svg, {'width':svgWidth, 'height':svgHeight});
var margin = 50;
var plotWidth = svgWidth - margin * 2;
var plotHeight = svgHeight - margin * 2;

// Background
addElement(svg, 'rect', {'id':'background', 'width':svgWidth, 'height':svgHeight, 'stroke':1, 'fill':'none'});

// Plot parent
var x = margin;
var y = svgHeight - margin;
var transform = 'translate({x}, {y})';
transform = transform.replace('{x}', x).replace('{y}', y);
var plotGroup = addElement(svg, 'g', {'id':'plot-group', 'transform':transform});

// Title
x = plotWidth / 2;
y =  - plotHeight - margin / 2;
addElement(plotGroup, 'text', {'id':'title', 'font-family':'Arial', 'dominant-baseline':'middle', 'text-anchor':"middle", 'font-size':24, 'x':x, 'y':y}, 'Camera Performance Envelopes');

// Plot Boundary
x = 0;
y = -plotHeight;
addElement(plotGroup, 'rect', {'id':'boundary', 'x':x, 'y':y, 'width':plotWidth, 'height':plotHeight, 'fill':'none', 'stroke':'black'});

// Y Axis label
transform = 'translate({x}, {y}) rotate({r})';
x = -margin * 0.65;
y = -plotHeight / 2;
var r = -90;
transform = transform.replace('{x}', x).replace('{y}', y).replace('{r}', r);
var yAxisLabelGroup = addElement(plotGroup, 'g', {'transform':transform});
addElement(yAxisLabelGroup, 'text', {'id':'title', 'font-family':'Arial', 'text-anchor':"middle", 'font-size':20}, "Megapixels");

// X Axis label
x = plotWidth / 2;
y = margin * 0.8;
var transform = 'translate({x}, {y})';
transform = transform.replace('{x}', x).replace('{y}', y);
var xAxisLabelGroup = addElement(plotGroup, 'g', {'transform':transform});
addElement(xAxisLabelGroup, 'text', {'id':'title', 'font-family':'Arial', 'text-anchor':"middle", 'font-size':20}, "Frames per second");


// Log bounds, range, scale
const xLog10Bounds = [1.0, 1000.0];
const xLog10Range = Math.log10(xLog10Bounds[1]) - Math.log10(xLog10Bounds[0]);
const xLog10Scale = plotWidth / xLog10Range;

const yLog10Bounds = [1.0, 100.0];
const yLog10Range = Math.log10(yLog10Bounds[1]) - Math.log10(yLog10Bounds[0]);
const yLog10Scale = plotHeight / yLog10Range;


function addLog10Grid () {
	var log10GridGroup = addElement(plotGroup, 'g', {'id':'log10-grid-group'});

	// X Axis Log10 Grid
	for (i = 1; i <= 10; i++) {
		for (j = 0; j <= 2; j++) {
			var x = Math.log10(i * Math.pow(10, j)) * xLog10Scale;
			addElement(log10GridGroup, 'line', {'x1':x, 'y1':0, 'x2':x, 'y2':-plotHeight, 'stroke':'black', 'stroke-width':0.1});
		}
	}

	// X Axis landmarks
	let landmarks = [1, 5, 10, 20, 30, 60, 120, 240, 1000];
	for (const landmark of landmarks){
		var x = Math.log10(landmark) * xLog10Scale;
		var labelText = landmark;
		addElement(log10GridGroup, 'line', {'x1':x, 'y1':0, 'x2':x, 'y2':margin * 0.1, 'stroke':'black', 'stroke-width':2});
		addElement(log10GridGroup, 'text', {'x':x, 'y':margin * 0.4, 'font-family':'Arial', 'text-anchor':"middle", 'font-size':14}, labelText);
	}

	// Y Axis Log10 Grid
	for (i = 1; i <= 10; i++) {
		for (j = 0; j <= 1; j++) {
			var y = Math.log10(i * Math.pow(10, j)) * yLog10Scale;
			var labelText = i * Math.pow(10, j);
			addElement(log10GridGroup, 'line', {'x1':0, 'y1':-y, 'x2':plotWidth, 'y2':-y, 'stroke':'black', 'stroke-width':0.1});

			if (i % 2 == 0) {
				addElement(log10GridGroup, 'line', {'x1':0, 'y1':-y, 'x2':-margin * 0.1, 'y2':-y, 'stroke':'black', 'stroke-width':2});
				addElement(log10GridGroup, 'text', {'x':-margin * 0.2, 'y':-y, 'font-family':'Arial', 'dominant-baseline':'middle', 'text-anchor':"end", 'font-size':14}, labelText);
			}
		}
	}
	return log10GridGroup;
}


// Make the grid
var log10Grid = addLog10Grid();
log10Grid.style.visibility = 'hidden';
// log10Grid.style.visibility = 'visible';


// Lin bounds, range, scale
const xBounds = [0.0, 260.0];
const xRange = xBounds[1] - xBounds[0];
const xScale = plotWidth / xRange;

const yBounds = [0.0, 50.0];
const yRange = yBounds[1] - yBounds[0];
const yScale = plotHeight / yRange;


function addLinGrid () {
	var linGridGroup = addElement(plotGroup, 'g', {'id':'lin-grid-group'});

	// // X Axis Lin Grid
	// var n = 13;
	// for (i = 0; i < n; i++) {
	// 	var x = i * (xRange / n) * xScale;
	// 	addElement(linGridGroup, 'line', {'x1':x, 'y1':0, 'x2':x, 'y2':-plotHeight, 'stroke':'black', 'stroke-width':0.1});
	// }

	// // X Axis landmarks
	// let landmarks = [10, 20, 30, 60, 120, 240];
	// for (const landmark of landmarks){
	// 	var x = landmark * xScale;
	// 	var labelText = landmark;
	// 	addElement(linGridGroup, 'line', {'x1':x, 'y1':0, 'x2':x, 'y2':margin * 0.1, 'stroke':'black', 'stroke-width':2});
	// 	addElement(linGridGroup, 'text', {'x':x, 'y':margin * 0.4, 'font-family':'Arial', 'text-anchor':"middle", 'font-size':14}, labelText);
	// }

	// X Axis Log10 Grid
	for (i = 1; i <= 10; i++) {
		for (j = 0; j <= 2; j++) {
			var x = Math.log10(i * Math.pow(10, j)) * xLog10Scale;
			addElement(linGridGroup, 'line', {'x1':x, 'y1':0, 'x2':x, 'y2':-plotHeight, 'stroke':'black', 'stroke-width':0.1});
		}
	}

	// X Axis landmarks
	let landmarks = [1, 5, 10, 20, 30, 60, 120, 240, 1000];
	for (const landmark of landmarks){
		var x = Math.log10(landmark) * xLog10Scale;
		var labelText = landmark;
		addElement(linGridGroup, 'line', {'x1':x, 'y1':0, 'x2':x, 'y2':margin * 0.1, 'stroke':'black', 'stroke-width':2});
		addElement(linGridGroup, 'text', {'x':x, 'y':margin * 0.4, 'font-family':'Arial', 'text-anchor':"middle", 'font-size':14}, labelText);
	}

	// Y Axis Lin Grid
	var n = 10;
	for (i = 0; i <= n; i++) {
		var y = i * (yRange / n) * yScale;
		var labelText = i * (yRange / n);
		addElement(linGridGroup, 'line', {'x1':0, 'y1':-y, 'x2':plotWidth, 'y2':-y, 'stroke':'black', 'stroke-width':0.1});

		if (i % 2 == 0) {
			addElement(linGridGroup, 'line', {'x1':0, 'y1':-y, 'x2':-margin * 0.1, 'y2':-y, 'stroke':'black', 'stroke-width':2});
			addElement(linGridGroup, 'text', {'x':-margin * 0.2, 'y':-y, 'font-family':'Arial', 'dominant-baseline':'middle', 'text-anchor':"end", 'font-size':14}, labelText);
		}
	}
	return linGridGroup;
}


// Make the Log10 grid
var log10Grid = addLog10Grid();

// Make the Lin grid
var linGrid = addLinGrid();

// Add an area and a curve plot for every camera to the cameras dict
for (var i = 0; i < cameras.length; i++) {
	var color = rgbString(palette[i]);
	var camera = cameras[i]

	var envelopeGroup = addElement(plotGroup, 'g', {'class':'envelope', 'id':camera.name});
	camera['envelope-group'] = envelopeGroup;

	camera['area'] = addElement(envelopeGroup, 'polygon', {'class':'area', 'points':'', 'fill':color, 'stroke':'none'});
	camera['curve'] = addElement(envelopeGroup, 'polyline', {'class':'curve', 'points':'', 'fill':'none', 'stroke-width':3, 'stroke':color});
}

// Populate the legend the camera entries
var legend = document.getElementById('legend');
for (var i = 0; i < cameras.length; i++) {
	var color = rgbString(palette[i])
	var camera = cameras[i];

	// Camera entry div
	var cameraEntry = document.createElement('div');
	cameraEntry.className = 'camera-entry';
	cameraEntry.setAttribute('id', camera.name);
	legend.appendChild(cameraEntry);

	// Checkbox with Camera Name
	addCheckboxAndLabel(cameraEntry, 'camera-checkbox', camera.name);

	cameraEntry.appendChild(document.createElement("br"));

	// Camera properties div
	var properties = document.createElement('div');
	properties.className = 'properties';
	cameraEntry.appendChild(properties);
	addColorSwatch(properties, color);
	properties.appendChild(document.createTextNode(cameras[i].price + ' - ' + camera.sensor));
}

//Footer text
var footer = document.getElementById('plot-footer');
var footerText = document.createElement('h3');
footerText.className = "footer-title"
var footerSubtext = document.createElement('p');
footer.appendChild(footerText);
footer.appendChild(footerSubtext);

// Add filter options
var filters = document.getElementById('filters');
addButton(filters, 'select-all-button', 'filter-button', filterCameras, 'All');
filters.appendChild(document.createElement('br'));
addButton(filters, 'select-none-button', 'filter-button', filterCameras, 'None');
filters.appendChild(document.createElement('br'));
addButton(filters, 'select-full-frame-button', 'filter-button', filterCameras, 'Full frame');
filters.appendChild(document.createElement('br'));
addButton(filters, 'select-apsc-button', 'filter-button', filterCameras, 'APS-C');
filters.appendChild(document.createElement('br'));
addButton(filters, 'select-mft-button', 'filter-button', filterCameras, 'Micro 4/3');

// Go through all entries and set hover and checkbox callbacks
var entries = legend.getElementsByClassName('camera-entry');
for (var i = 0; i < entries.length; i++) {
	var entry = entries[i]
	// Callback for when mouse hovers on/off
	entry.addEventListener('mouseover', onMouseOver);
	entry.addEventListener('mouseout', onMouseOut);

	// Callback for when a camera checkbox is checked
	var checkbox = entry.getElementsByClassName('camera-checkbox')[0];
	checkbox.onchange = onCheckbox;

	checkbox.checked = true;
}

// Interface to toggle between log and lin plots
var plotTypeSelect = document.getElementById('plot-type-select')
plotTypeSelect.addEventListener('change', updatePlotType);
updatePlotType();


function replotEnvelopesLinear() {
	for (var i = 0; i < cameras.length; i++) {
		updateEnvelopePlot(cameras[i], true);
	}
}

function replotEnvelopesLogarithmic() {
	for (var i = 0; i < cameras.length; i++) {
		updateEnvelopePlot(cameras[i], false);
	}
}

function updateEnvelopePlot(camera, linear) {
	// If `linear` is true plot lin-log else plot log-log

	var settings = camera.data;
	var points = '0,0 ';

	var prevX = 0;
	var prevY = 0;

	for (var i = 0; i < settings.length; i++) {
	    var setting = settings[i];
	    var x;
	    var y;

	    if (linear) {
			// x = setting[1] * xScale;
			x = Math.log10(setting[1]) * xLog10Scale;
			y = setting[0] * yScale;
		} else {
			x = Math.log10(setting[1]) * xLog10Scale;
			y = Math.log10(setting[0]) * yLog10Scale;
		}
		points += prevX + ',' + -y + ' '; // left of current
		points += x + ',' + -y + ' '; // current

		prevX = x;
		prevY = y;

		if (i == settings.length - 1) {
			points += x + ',' + 0 + ' '; // down to x axis to complete the polygon
		}
	}
	camera['area'].setAttribute('points', points);
	camera['curve'].setAttribute('points', points.slice(4));

/*
// Future reference for manipulating points directly
var svg = document.getElementById('svg');
var point = svg.createSVGPoint();
point.x = 10;
point.y = 20;
var polyline= document.getElementById('polyline-id');
polyline.points.appendItem(point);
*/
}

function updatePlotType(e) {
	var linearRadioButton = document.getElementById('lin-log-radio-button');
	var logarithmicRadioButton = document.getElementById('log-log-radio-button');

	if (linearRadioButton.checked) {
		linGrid.style.visibility = 'visible';
		log10Grid.style.visibility = 'hidden';
		replotEnvelopesLinear();
	}
	if (logarithmicRadioButton.checked) {
		linGrid.style.visibility = 'hidden';
		log10Grid.style.visibility = 'visible';
		replotEnvelopesLogarithmic();
	}
}


function onCheckbox() {
	var id = this.getAttribute('id');
	setEnvelopeVisibility(id, this.checked);
}


function setEnvelopeVisibility(id, visibility) {
	var envelope = svg.getElementById(id);
	var curve = envelope.getElementsByClassName('curve')[0];
	var area = envelope.getElementsByClassName('area')[0];

	if (visibility) {
		curve.style.visibility = 'visible';
		area.style.visibility = 'visible';
	} else {
		curve.style.visibility = 'hidden';
		area.style.visibility = 'hidden';
	}
}

function filterCameras() {
	var checkboxes = legend.getElementsByClassName('camera-checkbox');

	if (this.id == 'select-all-button') {
		for (checkbox of checkboxes) {
			checkbox.checked = true;
			setEnvelopeVisibility(checkbox.id, true);
		}
	} else if (this.id == 'select-none-button') {
		for (checkbox of checkboxes) {
			checkbox.checked = false;
			setEnvelopeVisibility(checkbox.id, false);
		}
	} else if (this.id == 'select-full-frame-button') {
		for (checkbox of checkboxes) {
			var keep = (getCamera(checkbox.id).sensor == 'Full frame');
			checkbox.checked = keep;
			setEnvelopeVisibility(checkbox.id, keep);
		}
	} else if (this.id == 'select-apsc-button') {
		for (checkbox of checkboxes) {
			var keep = (getCamera(checkbox.id).sensor == 'APS-C');
			checkbox.checked = keep;
			setEnvelopeVisibility(checkbox.id, keep);
		}
	} else if (this.id == 'select-mft-button') {
		for (checkbox of checkboxes) {
			var keep = (getCamera(checkbox.id).sensor == 'Micro 4/3');
			checkbox.checked = keep;
			setEnvelopeVisibility(checkbox.id, keep);
		}
	}
}

function onMouseOver() {
	// onMouseOut();
	this.setAttribute('style', 'font-weight:bold');

	var id = this.getAttribute('id');
	var envelope = svg.getElementById(id);
	var curve = envelope.getElementsByClassName('curve')[0];
	var area = envelope.getElementsByClassName('area')[0];

	curve.style.opacity = 1.0;
	area.style.opacity = 0.6;

	// Redraw on top
	plotGroup.removeChild(envelope);
	plotGroup.appendChild(envelope);
	plotGroup.insertBefore(envelope, null);

	populateFooter(id);
}

function onMouseOut() {
	this.setAttribute('style', 'font-weight:normal');

	var id = this.getAttribute('id');
	var envelope = svg.getElementById(id);
	var curve = envelope.getElementsByClassName('curve')[0];
	var area = envelope.getElementsByClassName('area')[0];

	curve.style.opacity = 0.6;
	area.style.opacity = 0.0;

	clearFooter();
}

function getCamera(name) {
	for (camera of cameras) {
		if (camera.name == name) {
			return camera;
		}
	}
}


function populateFooter(id) {
	var camera = getCamera(id);

	footerText.innerHTML = id;

	var subtext = '';
	for(setting of camera.data) {
		subtext += setting[0] + " MP @ " + setting[1] + 'fps' + (setting[2] ? ' (' + setting[2] + ')' : '') + '<br>';

	}
	footerSubtext.innerHTML = subtext;

}

function clearFooter() {
	footerText.innerHTML = '';
	footerSubtext.innerHTML = '';
}


function rgbaString(rgb, a){
	return "rgba(" + rgb[0] + ", " + rgb[1] + ", " + rgb[2] + ", " + a + ")";
}


function rgbString(rgb, a){
	return "rgb(" + rgb[0] + ", " + rgb[1] + ", " + rgb[2] + ")";
}

function addColorSwatch(parent, color) {
	var shape = document.createElementNS(ns, 'svg');
	setAttributes(shape, {'width':45, 'height':10});

	var style = 'fill:' + color + ';stroke:none';
	// var style = 'fill:green;stroke:none';

	var rectangle = document.createElementNS(ns, 'rect');
	setAttributes(rectangle, {'width':20, 'height':8, 'x':20, 'y':0, 'style':style})
	shape.appendChild(rectangle);

	parent.appendChild(shape);
}

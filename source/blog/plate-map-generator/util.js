
function ucFirst(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

function multiply(array) {
  // Returns the product of elements of an array
  if (array.length == 0) {
    return 0;
  }
  var product = 1;
  for (var i = 0; i < array.length; i++) {
    product = product * array[i];
  }
  return product;
}

function repeatArray(array, n){
  var result = [];
  for(var i = 0; i < n; i++) {
      result = result.concat(array);
  }
  return result;
}

function addIntegerSelector(parent, min, max, className) {
  var intSelector = document.createElement("select");
  intSelector.className = className
  for (i = min; i <= max; i++) {
    var option = document.createElement("option");
    option.value = i;
    option.innerHTML = i;
    intSelector.appendChild(option);
  }
  parent.appendChild(intSelector);

  return intSelector;
}

function addSelector(parent, options, className) {
  var selector = document.createElement("select");
  selector.className = className
  for (i = 0; i < options.length; i++) {
    var option = document.createElement("option");
    option.value = options[i];
    option.innerHTML = options[i];
    selector.appendChild(option);
  }
  parent.appendChild(selector);

  return selector;
}

function addCheckboxWithLabel(parent, className, labelMsg) {
  var checkbox = document.createElement('input');
  checkbox.type = "checkbox";
  checkbox.name = labelMsg;
  checkbox.className = className

  var label = document.createElement("label")
  label.appendChild(checkbox)
  label.appendChild(document.createTextNode(labelMsg));

  parent.appendChild(label);

  return checkbox
}

function getNumLevels(parameterFieldset) {
  // Returns the number of levels in the parameter fieldset whether it is
  // a numeric parameter or a typed parameter
  intSelector = parameter.getElementsByClassName("num-levels-select")[0]
  result = intSelector.value
  return result
}

function mapper(obj, item) {
  obj[item.treatment] = item.level;
  obj["blockDescription"] = item.blockDescription;
  return obj;
}

function flattenIntoSingleObject(a) {
  // Takes something like this
  //  [{"treatment":"Cumin", "level":0.5},
  //   {"treatment":"Corander", "level":2.5},
  //   {"treatment":"Paprika", "level":0.05},
  //   ]

  // And turns it into this
  //  {"Cumin": 0.5, "Coriander": 2.5, "Paprika": 0.05}
  if (!(Array.isArray(a))) {
    var result = mapper({}, a);
    return result ;
  }

  var result = a.reduce(mapper, {});
  return result;
}

function selectElementContents(el) {
  // From https://stackoverflow.com/a/42210996
  var body = document.body, range, sel;
  if (document.createRange && window.getSelection) {
    range = document.createRange();
    sel = window.getSelection();
    sel.removeAllRanges();
    try {
      range.selectNodeContents(el);
      sel.addRange(range);
    } catch (e) {
      range.selectNode(el);
      sel.addRange(range);
    }
    document.execCommand("copy");

  } else if (body.createTextRange) {
    range = body.createTextRange();
    range.moveToElementText(el);
    range.select();
    range.execCommand("Copy");
  }
}

function shuffleTableRows(table) {
  // Code modified from https://www.frankmitchell.org/2015/01/fisher-yates/

  var tableBody = table.getElementsByTagName('tbody')[0];

  // For debug
  var a = []
  for (i = 0; i < table.rows.length; i++) {
    a.push(i)
  }

  var i = 0, j = 0, temp = null

  for (var i = table.rows.length - 1; i > 0; i -= 1) {
    j = Math.floor(Math.random() * (i)) + 1

    // Swap row i with row j
    tableBody.insertBefore(table.rows[j], table.rows[i]);
    tableBody.insertBefore(table.rows[i], table.rows[j]);

    // For debug
    temp = a[j]
    a[j] = a[i]
    a[i] = temp
  }
  // console.log(a)
}

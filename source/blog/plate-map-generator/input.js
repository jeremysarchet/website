
function init() {

  // Set the number of reserved wells based on the config
  var previewTable = document.getElementById("preview");
  previewTable.rows[1].cells[3].innerHTML = RESERVED_WELLS.length

  // Callback for when add block button is clicked
  document.getElementById("append-block-button").onclick = addBlockFieldset;

  // Start out with a single block
  addBlockFieldset();

  // Callback for when copy to clipboard button is clicked
  document.getElementById("copy-output-button").onclick = function () {
    selectElementContents(document.getElementById("output-table"));
    alert("Copied to clipboard");
  };

  // Callback for when shuffle button is clicked
  document.getElementById("shuffle-output-button").onclick = function () {
    var table = document.getElementById("output-table");
    shuffleTableRows(table);
  };

  // Callback for when unshuffle button is clicked
  document.getElementById("unshuffle-output-button").onclick = updateOutput;
}

window.onload = init;

// window.onbeforeunload = function () {
//     return "Your plate design will be lost, are you sure?";
// }

function ensureAtLeastOneElement(parent, className) {
  var elements = parent.getElementsByClassName(className);

  if (elements.length) {
    return;
  } else {
    addBlockFieldset()
  }
}

function renumberElements(parent, className) {
  // Go through and get the text node for each and overwrite it
  elements = parent.getElementsByClassName(className);
  for (i = 0; i < elements.length; i++) {
    legend = elements[i].getElementsByTagName('legend')[0];
    elementName = legend.childNodes[0];
    elementName.nodeValue = ucFirst(className) + " " + (i + 1) + " ";
  }
}

function adjustNumberOfLevels(parent, name, n) {
  var values = PARAMETERS[name]
  var levelField

  // Wipe out existing content
  parent.innerHTML = ""
  for (var i = 1; i <= n; i++) {
    if (values == "Numeric") {
      levelField = document.createElement("input");
      levelField.className = "level-value"
      levelField.type = "number";
      parent.appendChild(levelField)

    } else {
      levelField = addSelector(parent, values, "level-value")
    }
    // Callback for when field value is changed
    levelField.onchange = updateOutput;

  }
}

function addBlockFieldset() {
  var parent = document.getElementById("blocks")

  // Add a fieldset
  var fieldset = document.createElement("fieldset");
  fieldset.className = "block";

  // Add a legend
  var legend = document.createElement("legend");
  fieldset.appendChild(legend);

  // Add a text node to the legend for the name
  legend.appendChild(document.createTextNode(""));

  // Button to remove the fieldset
  var  deleteButton = document.createElement("button");
  deleteButton.type = "button";
  deleteButton.className = "delete";
  deleteButton.onclick = function () {
    parent.removeChild(fieldset);
    ensureAtLeastOneElement(parent, "block");
    renumberElements(parent, "block");
    updateOutput();
  }
  deleteButton.innerHTML = "&times";
  legend.appendChild(deleteButton);

  // Add the contents of this parameter
  addBlockContents(fieldset);

  // Set up the block with appropriate parameters
  updateParameterFieldsets(fieldset)

  // Append the fieldset to the parent
  parent.appendChild(fieldset);
  // parent.appendChild(document.createElement("br"));

  // Revise the parameter numbering
  renumberElements(parent, "block");

  // Update the preview stats
  // TODO: see about this being double called when adding a block
  // it is ok because it is idempotent, but could be a performance issue
  updateOutput();
  // }

  return fieldset
}

function addBlockContents(parent) {
  // Selector for block description
  parent.appendChild(document.createTextNode("Type of block"));
  var descriptionSelector = addSelector(parent,
                                        Object.keys(BLOCK_DESCRIPTIONS),
                                        "description-selector");
  descriptionSelector.onchange = function () {
    updateBlockTreatmentFieldset(parent, descriptionSelector.value)
  };
  parent.appendChild(document.createElement("br"));

  // Add a fieldset for choosing subset of parameters
  var fieldset = document.createElement("fieldset");
  fieldset.className = "treatment";
  parent.appendChild(fieldset);

  // Legend
  var legend = document.createElement("legend");
  fieldset.appendChild(legend);
  legend.innerHTML = "Treatment";

  // Div for the parameter checkboxes
  var parametersCheckboxes = document.createElement("div");
  parametersCheckboxes.className = "parameters-checkboxes";
  fieldset.appendChild(parametersCheckboxes);

  // Div for the dynamic contents, the parameter fieldsets
  var parametersParent = document.createElement("div");
  parametersParent.className = "parameters"
  parent.appendChild(parametersParent)

  // Replicates
  parent.appendChild(document.createTextNode("Replicates: "));
  var intSelector = addIntegerSelector(parent, 1, 5, "num-replicates-select");

  // Callback for when num replicates selector is changed
  intSelector.onchange = function () {
    updateOutput();
  }

  parent.appendChild(document.createElement("br"));

  // Start off by defaulting to the first block description among the choices
  updateBlockTreatmentFieldset(parent, descriptionSelector.value)
}

function updateBlockTreatmentFieldset(parent, description) {
  var treatmentFieldset = parent.getElementsByClassName("treatment")[0];
  var parametersCheckboxes = treatmentFieldset.getElementsByClassName("parameters-checkboxes")[0];
  parametersCheckboxes.innerHTML = ""
  var parameterOptions = BLOCK_DESCRIPTIONS[description]

  // Populate fieldset with parameters checkboxes
  for (i = 0; i < parameterOptions.length; i++) {
    var newCheckbox = addCheckboxWithLabel(parametersCheckboxes, "parameter-checkbox", parameterOptions[i])
    newCheckbox.checked = true
    parametersCheckboxes.appendChild(document.createElement("br"));
    newCheckbox.onchange = function () {
      updateParameterFieldsets(parent)
    }
  }

  // Start off with all parameters for the description
  updateParameterFieldsets(parent)
}

function updateParameterFieldsets(block) {
  // Get handles to the checkboxes on this block
  var parametersFieldset = block.getElementsByClassName("parameters-checkboxes")[0];
  var checkboxes = parametersFieldset.getElementsByClassName("parameter-checkbox");

  // First, delete all existing parameter fieldsets
  var parametersParent = block.getElementsByClassName("parameters")[0];
  parametersParent.innerHTML = ""

  // Then add new parameter fieldsets according to which are checked up top
  for (var i = 0; i < checkboxes.length; i++) {
    if (checkboxes[i].checked) {
      addParameterFieldset(parametersParent, checkboxes[i].name)
    }
  }

  // Finally update the output
  updateOutput();
}

function anyParametersSelected() {
  // Returns true if any of the parameter checkboxes in any of the blocks
  // are checked else false
  return (getSelectedParameters().length > 0);
}

function getSelectedParameters() {
  var result = [];

  // Get handles to the blocks
  var blockParent = document.getElementById("blocks")
  var blocks = blockParent.children;

  // Make an object mapping parameters to booleans
  // Start off with all false
  var chosenParameters = JSON.parse(JSON.stringify(PARAMETERS))
  for (var i = 0; i < Object.keys(chosenParameters).length; i++) {
    chosenParameters[Object.keys(chosenParameters)[i]] = false
  }

  // Go through all the blocks and set to true the chosen parameters for each
  for (var i = 0; i < blocks.length; i++) {
    var block = blocks[i];
    var parametersFieldset = block.getElementsByClassName("parameters-checkboxes")[0];
    var checkboxes = parametersFieldset.getElementsByClassName("parameter-checkbox");
    for (var k = 0; k < checkboxes.length; k++) {
      if (checkboxes[k].checked) {
        chosenParameters[checkboxes[k].name] = true
      }
    }
  }

  // The result is a list of every key of the above object which is true
  var allParameters = Object.keys(PARAMETERS)
  for (var i = 0; i < allParameters.length; i++) {
    if (chosenParameters[allParameters[i]]) {
      result.push(allParameters[i])
    }
  }
  return result;
}

function getSelectedParameterNames() {
  result = getSelectedParameters()
  return result;
}

function addParameterFieldset(parent, name) {
  // Add a fieldset
  var fieldset = document.createElement("fieldset");
  fieldset.name = name
  fieldset.className = "parameter";

  // Add a legend
  var legend = document.createElement("legend");
  fieldset.appendChild(legend);

  // Add a text node to the legend for the name
  legend.appendChild(document.createTextNode(name));

  // Add the contents of this parameter
  addParameterContents(fieldset, name);

  // Append the fieldset to the parent
  parent.appendChild(fieldset);

  return fieldset
}

function addParameterContents(parent, name) {
  // Level values
  var levelsDiv = document.createElement("div");
  levelsDiv.className = "levels"
  parent.appendChild(levelsDiv)

  // Number of levels
  parent.appendChild(document.createTextNode("Number of levels: "));
  var intSelector = addIntegerSelector(parent, 1, 10, "num-levels-select");

  // Callback for when num levels selector is changed
  intSelector.onchange = function () {
    adjustNumberOfLevels(levelsDiv, name, intSelector.value);
    updateOutput();
  }
  // Start off with at least one levelsDiv
  adjustNumberOfLevels(levelsDiv, name, 1);
}


function updateOutput() {
  var blockParent = document.getElementById("blocks");
  var blocks = blockParent.children;

  var output = []

  if (anyParametersSelected()) {
    for (var i = 0; i < blocks.length; i++) {
      var block = blocks[i];
      var parameters = block.getElementsByClassName("parameters")[0].children;

      var blockData = []  // will be an array of parameterData arrays

      // Parameters
      for (var j = 0; j < parameters.length; j++) {
        parameter = parameters[j]
        treatment = parameter.name
        levels = parameter.getElementsByClassName("levels")[0].children;

        parameterData = [] // will be an array of objects like {"treatment":"Cumin", "level":"2.0"}

        for (var k = 0; k < levels.length; k++) {
          var newEntry = {}
          newEntry["treatment"] = treatment
          newEntry["level"] = levels[k].value ? levels[k].value : "?"
          newEntry["blockDescription"] = block.getElementsByClassName("description-selector")[0].value;

          // Append the entry to the parameter data
          parameterData.push(newEntry)
        }

        // Append the parameter data to the block data
        blockData.push(parameterData)
      }

      if (blockData.length > 0) {
        // Take the cartesian product
        blockData = cartesianProduct(...blockData)

        // Repeat the array N times from the number of replicates selected
        intSelector = block.getElementsByClassName("num-replicates-select")[0]
        numReplicates = intSelector.value
        blockData = repeatArray(blockData, numReplicates)
      }
      // Finally, append the block data to the total output
      output.push(blockData)
    }
  }

  // Finally, update the preview and output tables
  updatePreview(blockParent, blocks);
  updateOutputTable(output);
}


function updateOutputTable(output) {
  var table = document.getElementById("output-table");
  // Clear it out
  table.innerHTML = "";

  // Build the header
  var headerRow = table.insertRow(0);
  var headerValues = []
  headerValues.push("Index")
  headerValues.push("Type")
  headerValues.push("Concentration")
  headerValues.push("Block description");
  headerValues.push("Treatment")
  headerValues = headerValues.concat(getSelectedParameterNames())

  for (var i = 0; i < headerValues.length; i++) {
    var headerCell = document.createElement("th")
    headerCell.innerHTML = headerValues[i]
    headerRow.appendChild(headerCell)
  }

  // Populate the sample rows
  var block = ""
  var entry = ""
  for (var i = 0; i < output.length; i++) {
    var block = output[i]

    for (var j = 0; j < block.length; j++) {
      var entry = block[j]

      // Cast the entry into a new single object
      entry = flattenIntoSingleObject(entry)

      // Insert a new row at the end of the table for the new entry
      var newRow = table.insertRow(table.rows.length)

      // Populate each cell in the row
      newRow.insertCell(-1).innerHTML = table.rows.length - 1
      newRow.insertCell(-1).innerHTML = "Sample"
      newRow.insertCell(-1)

      // Block description cell
      var blockDescriptionCell = newRow.insertCell(-1);
      blockDescriptionCell.innerHTML = entry["blockDescription"]

      var treatmentCell = newRow.insertCell(-1);
      var treatmentMsg = []

      // Go through and populate the cells where values exist
      for (k = 0; k < getSelectedParameterNames().length; k++) {
        var newCell = newRow.insertCell(-1)
        var newVal = entry[getSelectedParameterNames()[k]]
        newCell.innerHTML = newVal ? newVal : ""

        // Build the array for the combined treatment in the first cell
        var treatment = getSelectedParameterNames()[k]
        if (newVal && (treatment != "Loading")) {
          // Special exception: don't include "Loading" as one of the treatments
          treatmentMsg.push(treatment)
        }

      // Populate the combined treatment cell
      treatmentCell.innerHTML = treatmentMsg.join(" + ");
      }
    }
  }

  // Populate the reserved wells
  for (var i = 0; i < RESERVED_WELLS.length; i++) {
    var newRow = table.insertRow(table.rows.length)
    // Populate each cell in the row
    newRow.insertCell(-1).innerHTML = table.rows.length - 1
    newRow.insertCell(-1).innerHTML = RESERVED_WELLS[i]["Type"]
    newRow.insertCell(-1).innerHTML = RESERVED_WELLS[i]["Concentration"]
    newRow.insertCell(-1); // note
    newRow.insertCell(-1);
    for (k = 0; k < getSelectedParameterNames().length; k++) {
      var newCell = newRow.insertCell(-1)
    }
  }
}


function updatePreview(blockParent, blocks){
  var previewTable = document.getElementById("preview");

  // First remove the existing table rows
  var blockRows = document.getElementsByClassName("block-preview")
  while(blockRows[0]) {
    blockRows[0].parentNode.removeChild(blockRows[0]);
  }

  for (var i = 0; i < blocks.length; i++) {
    var block = blocks[i];
    var parameters = block.getElementsByClassName("parameters")[0].children;

    // Add a new row third from last
    var newRow = previewTable.insertRow(previewTable.rows.length - 2);
    newRow.className = "block-preview";
    legend = block.getElementsByTagName('legend')[0].childNodes[0].nodeValue;
    newRow.insertCell(0).innerHTML = legend;

    // Parameters
    var numLevelsPerParameter = []
    for (var j = 0; j < parameters.length; j++) {
      parameter = parameters[j]
      var numLevels = getNumLevels(parameter)
      numLevelsPerParameter.push(numLevels)
    }
    if (parameters.length == 0) {
      newRow.insertCell(1).innerHTML = 0;
    } else {
      newRow.insertCell(1).innerHTML = numLevelsPerParameter.join(" &times ");
    }

    // Replicates
    intSelector = block.getElementsByClassName("num-replicates-select")[0]
    numReplicates = intSelector.value
    newRow.insertCell(2).innerHTML = numReplicates;

    // Subtotal
    var subtotal = multiply(numLevelsPerParameter) * numReplicates
    newRow.insertCell(3).innerHTML = subtotal;
  }

  // Finally, update the totals cells in the table
  updateTotals(previewTable);
}


function updateTotals(previewTable) {
  var total = 0;
  var numRows = previewTable.rows.length
  for (var k = 1; k <= numRows - 3; k++) {
    var summand = Number(previewTable.rows[k].cells[3].innerHTML)
    total += summand;
  }

  totalMsg = String(total) + " / 96"
  var cellTotal = previewTable.rows[numRows - 2].cells[3]
  var cellUsage = previewTable.rows[numRows - 1].cells[3]

  cellTotal.innerHTML = totalMsg
  cellUsage.innerHTML = parseInt(total / 96 * 100) + " %"

  if (total > 96) {
    cellTotal.style.color = "#f20000";
    cellUsage.style.color = "#f20000";
    cellTotal.style.fontWeight = "bold";
    cellUsage.style.fontWeight = "bold";
  } else {
    cellTotal.style.color = "black";
    cellUsage.style.color = "black";
    cellTotal.style.fontWeight = "normal";
    cellUsage.style.fontWeight = "normal";
  }
}

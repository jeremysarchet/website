// Cartesian Product of N arrays
// https://stackoverflow.com/questions/12303989/cartesian-product-of-multiple-arrays-in-javascript


// This code derived from the following, original two-liner
// https://stackoverflow.com/a/43053803
// let f = (a, b) => [].concat(...a.map(a => b.map(b => [].concat(a, b))));
// let cartesian = (a, b, ...c) => b ? cartesian(f(a, b), ...c) : a;
// Broken out a little to make it less fancy


// Helper function. Returns a 2d array of products
function productOfTwoArrays (a, b) {
  var result = [].concat(...
    a.map(
      a => b.map(
        b => [].concat(a, b)
      )
    )
  )
  return result;
}


// Returns an N dimensional array of products
// Accepts N arrays as arguments
function cartesianProduct(a, b, ...n) {  // "...n" is the "rest" of the arguments as a list
  if (b) {  // b  is undefined when passed in recursively as ""...n"
    var head = productOfTwoArrays(a, b)
    return cartesianProduct(head, ...n) // here "...n" is the "spread" of the rest of
                                 // the args, which is undefined if there aren't any
                                 // because the spread of an empty list is undefined
  } else {
    return a
  }
}


/*
Sample output:
let output = cartesianProduct([1, 2], [10, 20], [100, 200, 300]);
console.log(...output)

[ [ 1, 10, 100 ],
  [ 1, 10, 200 ],
  [ 1, 10, 300 ],
  [ 1, 20, 100 ],
  [ 1, 20, 200 ],
  [ 1, 20, 300 ],
  [ 2, 10, 100 ],
  [ 2, 10, 200 ],
  [ 2, 10, 300 ],
  [ 2, 20, 100 ],
  [ 2, 20, 200 ],
  [ 2, 20, 300 ] ]
  */

function test() {
  p1 = [{"Cumin": 0.5}, {"Cumin": 1}, {"Cumin": 1.5}]
  p2 = [{"Coriander": 2.5}, {"Coriander": 3}, {"Coriander": 3.5}]
  p3 = [{"Paprika": 0.05}, {"Paprika": 0.1}, {"Paprika": 0.15}]

  output = cartesianProduct(p1, p2, p3)

  // Output might look like this
  block1 = [
    {"treatment":"Cumin", "levels":[0.5, 1, 1.5]},
    {"treatment":"Coriander", "levels":[2.5, 3, 3.5]},
    {"treatment":"Paprika", "levels":[0.05, 0.1, 0.15]},
  ]

  // Get it into individual levels
  param1 = [{"treatment":"Cumin", "level":0.5},
            {"treatment":"Cumin", "level":0.1},
            {"treatment":"Cumin", "level":1.5},
          ]


  // Run it through the cartesian product
  // cartesianProduct()
  entry1 = [{"treatment":"Cumin", "level":0.5},
            {"treatment":"Corander", "level":2.5},
            {"treatment":"Paprika", "level":0.05},
          ]

  // Flatten each entry into its own object
  // console.log(flattenIntoSingleObject(entry1))
  entries = [[{"treatment":"Cumin", "level":0.5},
            {"treatment":"Corander", "level":2.5},
            {"treatment":"Paprika", "level":0.05},
          ],
          [{"treatment":"Cumin", "level":5},
          {"treatment":"Corander", "level":25},
          {"treatment":"Paprika", "level":0.5},
          ]]

  console.log(entries)
  var result = entries.map(flattenIntoSingleObject)
  console.log(result)
}

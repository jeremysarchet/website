
const PARAMETERS = {
  "Enzyme type":["Enzyme A", "Enzyme B"],
  "Detergent type":["SDS", "Triton X-100", "Tween-20"],
  "Salt type":["NaCl", "KCl", "NaBr"],
  "Solvent type":["Ethanol", "Acetone"],
  "Acid type":["HCl", "Citric acid", "Acetic acid"],
  "Base type":["NaOH", "KOH"],
  "Enzyme loading":"Numeric",
  "Detergent conc.":"Numeric",
  "Salt conc.":"Numeric",
  "Solvent conc.":"Numeric",
  "pH":"Numeric",
  "Ionic strength":"Numeric",
}

const BLOCK_DESCRIPTIONS = {
  "Enzyme":["Enzyme type", "Enzyme loading", "pH"],
  "Detergent":["Detergent type", "Detergent conc.", "pH", "Ionic strength"],
  "Salt":["Salt type", "Salt conc.", "pH"],
  "Solvent":["Solvent type", "Solvent conc.", "pH"],
  "Titration curve":["pH"],
  "Multiple":Object.keys(PARAMETERS), // all the parameters
}

const RESERVED_WELLS = [
  {"Type":"Standard", "Concentration":"1500"},
  {"Type":"Standard", "Concentration":"1500"},
  {"Type":"Standard", "Concentration":"1000"},
  {"Type":"Standard", "Concentration":"1000"},
  {"Type":"Standard", "Concentration":"500"},
  {"Type":"Standard", "Concentration":"500"},
  {"Type":"Standard", "Concentration":"250"},
  {"Type":"Standard", "Concentration":"250"},
  {"Type":"Blank", "Concentration":"0"},
  {"Type":"Blank", "Concentration":"0"},
  {"Type":"Blank", "Concentration":"0"},
  {"Type":"Blank", "Concentration":"0"},
  {"Type":"Control", "Concentration":"1000"},
  {"Type":"Control", "Concentration":"1000"},
  {"Type":"Control", "Concentration":"500"},
  {"Type":"Control", "Concentration":"500"},
]

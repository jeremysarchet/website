# Extract, parse, and cut metadate (YAML front matter) from a document

require "yaml"

module Metadata
	extend self

	# Pattern for yaml front matter in html files
	# The front matter is used by the site generator and stripped away
	FRONT_MATTER_PATTERN = /---[\s\S]*?---\n/

	def has_front_matter?(document)
		return FRONT_MATTER_PATTERN.match?(document)
	end

	def remove_front_matter(content_string)
		# Regex to recognize YAML Front Matter at beginning of file:
		# 3 hyphens, then any charecters, then 3 hyphens and a newline
		return content_string.gsub(FRONT_MATTER_PATTERN, "")
	end

	def split_and_parse_front_matter(document)
		# Cut the file via yaml to extract the metadata to a hash
		yaml = document[FRONT_MATTER_PATTERN] || ""
		front_matter = YAML.load(yaml) || {}
		# front_matter.transform_keys!(&:to_sym)
		content = remove_front_matter(document)
		return front_matter, content
	end

end

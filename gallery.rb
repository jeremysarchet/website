# Generate a gallery of images or videos

require "fileutils"
require "pathname"
require "yaml"


module Gallery
	extend self

	def generate(document, path)
		directory = Pathname.new(File.dirname(path))
		# asset_directory = Pathname.new(File.join(directory, "assets"))
		# asset_paths = asset_directory.children.select { |child| child.file? }
		# names = asset_paths.map { |n| File.join("assets", File.basename(n))}

		data_document = File.read(File.join(directory, "info.yaml"))
		data = YAML.load(data_document) || {}
		layout_data = data["layout"]
		artwork_data = data["artworks"]

		red_dot = "<span style='color: crimson; vertical-align: text-bottom'> &#9679</span>"
		# Single Column Gallery
		result = ""
		artwork_data.each do |entry|
			title_slug = entry["title"].gsub(" ", "-").downcase
			sold_indicator = entry.has_key?("sold") ? red_dot : nil
			width = entry["width"] ? "%04d" % entry["width"] : "1920"
			result << "<div id='#{title_slug}' class='artwork-item'>"
			if entry.has_key?("video")
				result << "<video #{entry["loop"]} class='artwork' poster='images/#{width}/#{entry["image"]}' preload='metadata' controls='controls'>
										<source type='video/mp4' src='videos/#{entry["video"]}'/>
									</video>"
			else
				result << "<img src='images/#{width}/#{entry["image"]}'>\n"
			end
			result << "<h3 name='#{title_slug}' class='artwork-title'>#{entry["title"]}#{sold_indicator}</h3>"
			result << "<p class='description-paragraph'>#{entry["description"]}</p>"
			result << "</div>"
		end
		document = document.gsub(/%{narrow-gallery}/, result)

		# Slideshow Gallery
		document = document.gsub(/%{aspect}/, layout_data["aspect"])
		document = document.gsub(/%{fraction}/, layout_data["fraction"])
		thumbnails = ""
		artworks = ""
		titles = ""
		descriptions = ""
		artwork_data.each do |entry|
			title_slug = entry["title"].gsub(" ", "-").downcase
			sold_indicator = entry.has_key?("sold") ? red_dot : nil
			thumbnails << "<img class='thumbnail' src='images/0640/#{entry["image"]}'>\n"
			width = entry["width"] ? "%04d" % entry["width"] : "1920"
			if entry.has_key?("video")
				artworks << "<video #{entry["loop"]} class='artwork' poster='images/#{width}/#{entry["image"]}' preload='metadata' controls='controls' style='display:none'>
											<source type='video/mp4' src='videos/#{entry["video"]}'/>
											</video>"
			else
				artworks << "<img class='artwork' src='images/#{width}/#{entry["image"]}' style='display:none'>\n"
			end
			titles << "<h3 name='#{title_slug}' class='artwork-title' style='display:none'>#{entry["title"]}#{sold_indicator}</h3>\n"
			descriptions <<  "<div class='description' style='display:none'>
													<p class='description-paragraph'>#{entry["description"]}</p>
												</div>"

		end
		document = document.gsub(/%{thumbnails}/, thumbnails)
		document = document.gsub(/%{artworks}/, artworks)
		document = document.gsub(/%{titles}/, titles)
		document = document.gsub(/%{descriptions}/, descriptions)

		return document
	end

end

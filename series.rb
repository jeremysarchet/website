# Html fragments to be used for a series of pages
# Generate a listing of links to pages
# For a given page, links to jump to the previous and next in the series

require "fileutils"
require "pathname"

EXCLUSIONS = ["(Unlisted)", "(Draft)"]

module Series
	extend self


	def read_and_get_metadata(parent_directory, dirname)
		document_path = File.join(parent_directory, dirname, "index.html")
		document = File.read(document_path)
		meta, content = Metadata.split_and_parse_front_matter(document)
		return meta
	end

	def get_sorted_list_items(directory)
		subdirectories = directory.children.select { |child| child.directory? }
		list_items = []
		subdirectories.each do |subdirectory|
			# Read the source file and get its metadata
			dirname = File.basename(subdirectory)
			meta = read_and_get_metadata(directory, dirname)

			# Don't include the page at all in the listing if it's a lone page
			if meta.key?("wrapping") and meta["wrapping"].include?("lone")
				next
			end

			# Collate the properties into a dict for sorting by date
			list_items << {"dirname" => dirname,
											"title" => meta["title"],
											"date" => meta["date"].to_s
										}
		end

		# Sort by date
		list_items.sort_by! { |entry| entry["date"] }

		# Exclude draft entries
		list_items.reject! { |entry| EXCLUSIONS.any? { |str| entry["title"].include?(str) } }


		return list_items
	end

	def generate_listing(path, with_thumbnails=true)
		directory = Pathname.new(File.dirname(path))
		list_items = get_sorted_list_items(directory)

		# Display listing with most recent on top
		list_items.reverse!

		# Generate the HTML for the listing entries
		result = "<ul class='listing'>"
		list_items.each do |list_item|
			link = "#{list_item["dirname"]}/%{postfix}"
			thumbnail = "
				<div class='listing-thumbnail-area'>
					<a class='listing-thumbnail' href='#{link}'>
						<img src='#{list_item["dirname"]}/thumbnail/270/post-thumbnail.jpg' width='100' height='100'>
					</a>
				</div>"
			result << "<li>
			<div class='listing-entry-area'>
				#{with_thumbnails ? thumbnail : nil}
				<div class='listing-label'>
					<a href='#{link}'>#{list_item["title"]}</a><br>
					#{list_item["date"]}
					<br>
				</div>
			</div>
			</li>"
		end
		result << "</ul>"
		return result
	end

	def generate_jumps(path, with_thumbnails=true)
		result = ""

		directory = Pathname.new(File.dirname(File.dirname(path)))
		list_items = get_sorted_list_items(directory)
		this_dirname = File.basename(File.dirname(path))

		# Don't generate jumps if it's an unlisted page
		meta = read_and_get_metadata(directory, this_dirname)

		if EXCLUSIONS.any? { |str| meta["title"].include?(str) }
			result << "This page is unlisted. Only those who know the link can view it."
			return result
		end

		this_item = list_items.find {|hsh| hsh["dirname"] == this_dirname}
		i = list_items.index(this_item)

		previous_item = i == 0 ? nil : list_items[i - 1]
		previous_title = ""
		previous_name = ""
		if previous_item
			previous_name = previous_item["dirname"]
			previous_title = previous_item["title"]
			meta = read_and_get_metadata(directory, previous_name)
			previous_title = meta["title"] || "Untitled"
		end

		next_item = i == list_items.size - 1 ? nil : list_items[i + 1]
		next_title = ""
		next_name = ""
		if next_item
			next_name = next_item["dirname"]
			next_title = next_item["title"]
			meta = read_and_get_metadata(directory, next_name)
			next_title = meta["title"] || "Untitled"
		end

		next_thumbnail = "
			<div class='listing-thumbnail-area'>
				<a class='listing-thumbnail' href='../#{next_name}/%{postfix}'><img src='../#{next_name}/thumbnail/270/post-thumbnail.jpg' width='100' height='100'></a>
			</div>"

		jump_next = "
			<div class='listing-entry-area'>
				#{with_thumbnails ? next_thumbnail : nil}
				<div class='listing-label'>
					<span class='prev-next'>Next:</span><br><a href='../#{next_name}/%{postfix}'>#{next_title}</a>
				</div>
			</div>"

		prev_thumbnail = "
			<div class='listing-thumbnail-area'>
				<a class='listing-thumbnail' href='../#{previous_name}/%{postfix}'><img src='../#{previous_name}/thumbnail/270/post-thumbnail.jpg' width='100' height='100'></a>
			</div>"

		jump_prev = "
			<div class='listing-entry-area'>
				#{with_thumbnails ? prev_thumbnail : nil}
				<div class='listing-label'>
					<span class='prev-next'>Previous:</span><br><a href='../#{previous_name}/%{postfix}'>#{previous_title}</a>
				</div>
			</div>"

		# Generate the HTML for the previous and next jump links
		result = "<ul class='listing'>"
		if list_items.size > 1
			if i == 0
				result << "<li>#{jump_next}</li>"
			elsif i == list_items.size - 1
				result << "<li>#{jump_prev}</li>"
			else
				result << "<li>#{jump_prev}</li><li>#{jump_next}</li>"
			end
		end

		if meta.key?("see-also")
			see_also_name = meta["see-also"]
			meta = read_and_get_metadata(directory, see_also_name)
			see_also_title = meta["title"] || "Untitled"

	    result << "<li>
	      <div class='listing-entry-area'>
	        <div class='listing-thumbnail-area'>
	          <a class='listing-thumbnail' href='../#{see_also_name}/%{postfix}'><img src='../#{see_also_name}/thumbnail/270/post-thumbnail.jpg' width='100' height='100'></a>
	        </div>
	        <div class='listing-label'>
	          <span class='prev-next'>See also:</span><br><a href='../#{see_also_name}/%{postfix}'>#{see_also_title}</a>
	        </div>
	      </div>
	    </li>"
		end

		result << "</ul>"

		return result
	end

end
